/* eslint-disable no-unused-vars */
import React  from 'react';
import { Routes, Route } from "react-router-dom"
import {Provider} from "react-redux"
import Login from "./components/login"
import store from "./helpers/store"
// import PrivateRoutes from './views/PrivateRoutes';

// import { AuthContextProvider } from './AuthContext';
 
 
import './App.css'


function App() {

  return (
    // <AuthContextProvider>
    <Provider store={store}>
    <div className="App" style={{height:"100%"}}>
    
        
     <Routes>
        {/* <Route element={<PrivateRoutes />}>

        </Route> */}
        <Route path="/login" element={ <Login /> }  />
  
  </Routes>
  </div>
 {/* </AuthContextProvider> */}
  </Provider>
  );
}

export default App;