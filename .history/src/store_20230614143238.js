import { createStore } from "redux";
import { cartReducer } from "./login/reducers/loginReducer";

const store = createStore(cartReducer);

export default store;