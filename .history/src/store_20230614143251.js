import { createStore } from "redux";
import { loginReducer } from "./login/reducers/loginReducer";

const store = createStore(loginReducer);

export default store;