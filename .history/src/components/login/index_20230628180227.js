/* eslint-disable no-useless-escape */
import React,{useState} from 'react';
import { useNavigate } from 'react-router-dom';
// import Cookies from 'js-cookie'
import { useSelector, useDispatch } from "react-redux";

import Group_1 from '../../assets/images/Group_1.png'
import FeeMonk_HighRes_Logo from '../../assets/images/FeeMonk_HighRes_Logo.png'
import './index.css'
import {userActions} from '../../actions/user.actions'

// import { Navigate } from 'react-router-dom';
// import {useUserAuth} from "../../AuthContext"

export default function Login() {

//   const {loggedIn} = useUserAuth();
  const navigate=useNavigate();
  const dispatch = useDispatch();
  useSelector((state) => {
    setUserDetails(state?.Inprogress?.applicants?.res?.applicantDetails)
      console.log("User Details", userDetails)
      setTableData(userDetails);
      setFilterTableData(userDetails);
  });
  // console.log("store", state);


  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')
  const [emailError,setEmailError]=useState('')
  const [passwordError,setPasswordError]=useState('')
 
  const validateEmail = email => {
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(email);
  };
  
  const validate = () => {
    
    
     if (password.length < 8) {
      setPasswordError("Password must be at least 8 characters long");
    }else{
      setPasswordError("");

    }

    if (passwordError) {
      setPasswordError(passwordError)
      return false;
    }

    return true;
  };
  
  
  
  


    
 
  
 const  onChangeEmail = event => {
    const email = event.target.value;
    const isValid = validateEmail(email);
    
    setEmail(email)
    setEmailError(!isValid ? "Invalid email address" : null)
   
  };
  
  

 const  onChangePassword = event => {
    setPassword(event.target.value)
    
  }
  
 const  submitForm = async event => {
    event.preventDefault()
    
    
    const userDetails = {email, password}

    const isValid =validate();
    if (isValid) { 
  
      if (userDetails) {

        dispatch(userActions.login(email, password, navigate))
        
        // loggedIn(url,options)
      
    }

  }
  }






    
    // const jwtToken = Cookies.get('jwt_token')
    // if (jwtToken !== undefined) {
    //   return <Navigate to="/" />
    // }
  
    return (
      <>
         <div className="login-form-container">
        <div className="login-background">
          {/* <div className="login-background-2"> */}
          <img src={Group_1} className="login-image" alt="login" />

          <img
            src={FeeMonk_HighRes_Logo}
            className="feemonk-image"
            alt="website logo"
            
          />

          <p className="caption">Simplified education fee payments & fiscal management</p>
        {/* </div> */}
        </div>
        <form className="form-container" >

          <p style={{margin:'0'}}>To get started</p>
          <h3 style={{color:'#D32028', fontFamily:"sans-serif", fontWeight:'bold'}}>Log In</h3>
         
          <div className="input-container">
            <label className="input-label" htmlFor="email">
          Email
        </label>
        <input
          type="text"
          id="email"
          className="email-input-filed"
          value={email}
          onChange={(e)=>onChangeEmail(e)}
        />
       
        {emailError && <div style={{ color: 'red' }}>{emailError}</div>}
        
     </div>
          <div className="input-container">

        <label className="input-label" htmlFor="password">
          Password
        </label>
        <input
          type="password"
          id="password"
          className="password-input-filed"
          value={password}
          onChange={(e)=>onChangePassword(e)}
        />
          {passwordError && <div style={{ color: 'red' }}>{passwordError}</div>}
        
      </div>
          <div className="buttons-container1">
              {/* onClick={()=>navigate('/forgotPassword')} */}
           <p  style={{color:'#D32028',textAlign:'right', cursor : 'pointer'}}>Forgot Password ?</p>
          <button  type="submit" className="login-button" onClick={(e)=>submitForm(e)}>
            Login
          </button>
          {/* <div style ={{display:'flex', flexDirection : 'row', alignContent: 'center'}}><p style={{marginTop : '20px' , marginRight : '10px'}}>Don't have an account? </p><SignUpForm /> </div> */}
         </div>
        
        </form>   
      </div>
      </>
    )
  
}


