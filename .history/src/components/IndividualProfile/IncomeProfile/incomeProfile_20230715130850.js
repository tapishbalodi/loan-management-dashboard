import React,{useState} from 'react';
import {
  
  Modal,
  ModalBody,
  Button,
  ModalHeader,
} from 'reactstrap';
import Switch from '@mui/material/Switch';
import successful from '../../../assets/images/successful.png'
import attention from '../../../assets/images/attention.png'
import credit5 from '../../../assets/images/Credit5.png'
import debit5 from '../../../assets/images/debit5.png'
import faceLive from '../../../assets/images/facelive.png'
import ckyckImage from '../../../assets/images/ckyckImage.png'
import documentImage from '../../../assets/images/documentSample.png'
import fraudCheck from '../../../assets/images/fraudCheck.png'
import uploadPdf from '../../../assets/images/uploadPdf.png'
import frame from '../../../assets/images/Frame.png'
import averageEod from '../../../assets/images/averageEod.png'
import ViewFile from '../Files/viewFile.js'
import {
  Chart as ChartJS,
  LinearScale,
  ArcElement,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController,
} from 'chart.js';


import { Chart,Bar ,Doughnut} from 'react-chartjs-2';
import './index.css'

ChartJS.register(
  ArcElement,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  LineController,
  BarController
);


export default function IncomeProfile({kycDetails,bankVerificationDetails,digilockerDetails,panDetails}) {


const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  const optionsVerticalBar = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' ,
    },
    title: {
      display: true,
       
    },
  },
};
const dataVertical = {
  labels,
  datasets: [
    {
      label: 'Dataset 1',
      data: [200,300,400,500,600,760,800],
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    },
     
  ],
};
  
  const dataDoughnut = {
  labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
  datasets: [
    {
      label: '# of Votes',
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
      ],
      borderWidth: 1,
    },
  ],
};

  const options = {
  indexAxis: 'y',
  elements: {
    bar: {
      borderWidth: 2,
    },
  },
  responsive: true,
  plugins: {
    legend: {
      position: 'right',
    },
    title: {
      display: true,
      text: 'Chart.js Horizontal Bar Chart',
    },
  },
};

 

const dataBar = {
  labels,
  datasets: [
    {
      label: 'Dataset 1',
      data: [200,300,400,500,600,760,800],
      borderColor: 'rgb(255, 99, 132)',
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    },
    
  ],
};


 const data = {
  labels,
  datasets: [
    {
      type: 'line'   ,
      label: 'Dataset 1',
      borderColor: 'rgb(255, 99, 132)',
      borderWidth: 2,
      fill: false,
      data: [250,440,330,500,750,900,550],
    },
    {
      type: 'bar' ,
      label: 'Dataset 2',
      backgroundColor: 'rgb(75, 192, 192)',
      data: [250,440,330,500,750,900,550],
      borderColor: 'white',
      borderWidth: 2,
    },
    {
      type: 'bar' ,
      label: 'Dataset 3',
      backgroundColor: 'rgb(53, 162, 235)',
      data: [250,440,330,500,750,900,550],
    },
  ],
};
   const [checked, setChecked] = React.useState(true);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };
   
  
  return (
    <div style={{display:'block'}}>
      <div style={{display:'flex',paddingTop:'1em',
      marginTop:'2em',justifyContent:'space-around',backgroundColor:'#FFF3F3',border:'1px solid #D32028',borderRadius:'1em'}}>
        <div style={{display:'block',lineHeight:'1em', }}>
          <p>Current Rent</p>
          <p style={{fontWeight:'bold'}}>35000</p>
        </div>
        <div style={{display:'block',lineHeight:'1em'}}>
          <p>Current Income</p>
          <p style={{fontWeight:'bold'}}>35000</p>
        </div>
        <div style={{display:'block',lineHeight:'1em'}}>
          <p>Current EMI</p>
          <p style={{fontWeight:'bold'}}>35000</p>
        </div>
        <div style={{display:'block',lineHeight:'1em'}}>
          <p>Feemonk EMI</p>
          <p style={{fontWeight:'bold'}}>35000</p>
        </div>
        <div style={{display:'block',lineHeight:'1em'}}>
          <p>FOIR</p>
          <p  style={{fontWeight:'bold'}}>35000</p>
        </div>
        
        <div style={{display:'block',lineHeight:'1em'}}>
          <Button style={{backgroundColor:'#D32028',border:'none'}}>Edit Foir</Button>
        </div>
        
      </div>
      <div style={{display:'flex',justifyContent: 'space-around',marginTop:'1em'}}>
        <div>

           <span style={{fontSize:'18px',fontWeight:'bold',color:'#D32028'}}>PDF View</span>
            <Switch
            checked={checked}
            onChange={handleChange}
            inputProps={{ 'aria-label': 'controlled' }}
          />
          <span style={{fontSize:'18px',fontWeight:'bold',color:'#D32028'}}>Account Aggregator</span>
         

        </div>
        <img style={{cursor:'pointer'}} src={uploadPdf}/>
      </div>
        <div style={{display:'flex',justifyContent: 'space-between',marginRight:'2em',marginLeft:'1em'}}>

            <div style={{border:'1px solid #D0D0D0',borderRadius:'1em',width:'50%',margin:'1em',boxShadow:'0px 1px 1px 1px  #C0C0C0'}}>
              <div style={{display:'flex',justifyContent:'space-around',}}>
                <div style={{display:'flex',justifyContent: 'space-between',padding:'1em',width:'100%',borderBottom:'1px solid #D0D0D0'}}>
                <p style={{fontSize:'18px',fontWeight:'bold',}}><img style={{marginRight:'0.5em'}} src={averageEod}/>Average EOD Balance</p>
                
                </div>

              </div>
               <div style={{padding:'1em',paddingLeft:'2em',marginTop:'2em'}}>
                              <Bar options={optionsVerticalBar} data={dataVertical} />

                </div>

            </div>
            <div style={{border:'1px solid #AEBBD7',borderRadius:'1em',width:'50%',margin:'1em',backgroundColor:'#F6F9FF',boxShadow:'0px 1px 1px 1px  #C0C0C0'}}>
              <div style={{display:'flex',justifyContent:'space-around',}}>
                <div style={{display:'flex',justifyContent: 'space-between',padding:'1em',width:'100%',borderBottom:'1px solid #D0D0D0'}}>
                <p style={{fontSize:'18px',fontWeight:'bold',}}><img style={{marginRight:'0.5em'}} src={fraudCheck}/>Fraud Checks</p>
                 
                </div>

              </div>
               <div style={{padding:'1em'}}>
               

                <div style={{display:'flex',justifyContent:'space-between',padding:'1em'}}>
                  <div style={{display:'block'}}>
                   <p style={{color:'#667085'}}>Face Match Score</p>
                    <p style={{fontWeight:'bold'}}>{bankVerificationDetails?.selfieDetails?.facematchScore} %</p>
                  </div>
                  <div style={{display:'block'}}>
                  <p style={{color:'#667085'}}>Accuracy</p>
                  <p style={{fontWeight:'bold'}}>{Math.round(bankVerificationDetails?.selfieDetails?.getTagAccuracy)}</p>
                  </div>
                
                </div>
                <div style={{display:'flex',justifyContent:'space-between',padding:'1em', }}>
                  <div style={{display:'block'}}>
                   <p style={{color:'#667085'}}>latitude</p>
                    <p style={{fontWeight:'bold'}}>{bankVerificationDetails?.selfieDetails?.getTagLatitute}</p>
                  </div>
                  <div style={{display:'block'}}>
                  <p style={{color:'#667085'}}>Longitude</p>
                  <p style={{fontWeight:'bold'}}>{bankVerificationDetails?.selfieDetails?.getTagLongitude}</p>
                  </div>
                
                </div>
                <div style={{display:'flex',justifyContent:'space-between',padding:'1em'}}>
                  <div style={{display:'block'}}>
                   <p style={{color:'#667085'}}>Geo-Address 
                   
                   <a  href={`https://maps.google.com/maps?q=${bankVerificationDetails?.selfieDetails?.geoTagLatitude},${bankVerificationDetails?.selfieDetails?.geoTagLongitude}&hl=es;z=14&amp;output=embed`}
                  style={{color:'#0000FF',backgroundColor:'aliceBlue',textAlign:"left" }}
                  target="_blank"
                  rel='noreferrer'>Open Map</a></p>

                  <p style={{fontWeight:'bold'}}>{bankVerificationDetails?.selfieDetails?.getTagAddress}</p>
                  </div>
                   
                
                </div>
          </div>

            </div>
        </div>
        <div style={{display:'flex',justifyContent: 'space-between',marginRight:'2em',marginLeft:'1em'}}>

            <div style={{border:'1px solid #D0D0D0',borderRadius:'1em',width:'50%',margin:'1em',boxShadow:'0px 1px 1px 1px  #C0C0C0'}}>
              <div style={{display:'flex',justifyContent:'space-around',}}>
                <div style={{display:'flex',justifyContent: 'space-between',padding:'1em',width:'100%',borderBottom:'1px solid #D0D0D0'}}>
                <p style={{fontSize:'18px',fontWeight:'bold'}}><img style={{marginRight:'0.5em'}} src={credit5}/>Top 5 Credit Transactions</p>
                 
                </div>

              </div>
            <div style={{display:'flex',justifyContent: 'space-between'}}>
              <Bar options={options} data={dataBar} />
            </div>

            </div>
            <div style={{border:'1px solid #D0D0D0',borderRadius:'1em',width:'50%',margin:'1em',boxShadow:'0px 1px 1px 1px  #C0C0C0'}}>
              <div style={{display:'flex',justifyContent:'space-around',}}>
                <div style={{display:'flex',justifyContent: 'space-between',padding:'1em',width:'100%',borderBottom:'1px solid #D0D0D0'}}>
                <p style={{fontSize:'18px',fontWeight:'bold',}}><img style={{marginRight:'0.5em'}} src={debit5}/>Top 5 Debit Transaction</p>
                
                </div>

              </div>
               <div style={{padding:'1em'}}>
               

                <div style={{display:'flex',justifyContent:'space-between',padding:'1em'}}>
                 <Bar options={options} data={dataBar} />
               
                </div>
               
          </div>

            </div>
        </div>
          
        
         <div style={{border:'1px solid #D0D0D0',borderRadius:'1em',width:'95%',marginLeft:'2em',boxShadow:'0px 1px 1px 1px  #C0C0C0'}}>
              <div style={{display:'flex',justifyContent:'space-around',}}>
                <div style={{display:'flex',justifyContent: 'space-between',padding:'1em',width:'100%',borderBottom:'1px solid #D0D0D0'}}>
                <p style={{fontSize:'18px',fontWeight:'bold',}}><img style={{marginRight:'0.5em'}} src={frame}/>FOIR Summary</p>
                 
                </div>

              </div>
               <div style={{padding:'1em'}}>
               

                <div style={{display:'flex',justifyContent:'space-between',padding:'1em'}}>
                  <Chart type='bar' data={data} />
               </div>
          </div>
        </div>
         <div style={{border:'1px solid #D0D0D0',borderRadius:'1em',width:'95%',marginLeft:'2em',boxShadow:'0px 1px 1px 1px  #C0C0C0',marginTop:'2em'}}>
              <div style={{display:'flex',justifyContent:'space-around',}}>
                <div style={{display:'flex',justifyContent: 'space-between',padding:'1em',width:'100%',borderBottom:'1px solid #D0D0D0'}}>
                <p style={{fontSize:'18px',fontWeight:'bold',}}><img style={{marginRight:'0.5em'}} src={frame}/>Income Summary</p>
                 
                </div>

              </div>
               <div style={{padding:'1em' }}>
               

                <div style={{display:'flex',justifyContent:'space-between',padding:'1em',paddingLeft:'5em',paddingRight:'5em'}}>
                  <div>
                  <Doughnut   data={dataDoughnut} />
                  </div>
                  <div style={{display:'block'}}>
                  <div style={{display:'flex',justifyContent:'space-around'}}>
                    <div style={{display:'block',padding:'1em',marginRight:'1em',border:'1px solid #D0D0D0',boxShadow:'0px 1px 1px 1px #D0D0D0',borderRadius:'10px'}}>
                      <p>Average Salary</p>
                      <p>54000</p>
                    </div>
                    <div style={{display:'block',marginLeft:'1em',padding:'1em',border:'1px solid #D0D0D0',boxShadow:'0px 1px 1px 1px #D0D0D0',borderRadius:'10px'}}>
                      <p>Average Salary</p>
                      <p>54000</p>
                    </div>
                  </div>
                  <div style={{display:'flex',justifyContent:'space-around',marginTop:'1em'}}>
                    <div style={{display:'block',padding:'1em',marginRight:'1em',border:'1px solid #D0D0D0',boxShadow:'0px 1px 1px 1px #D0D0D0',borderRadius:'10px'}}>
                      <p>Average Salary</p>
                      <p>54000</p>
                    </div>
                    <div style={{display:'block',marginLeft:'1em',padding:'1em',border:'1px solid #D0D0D0',boxShadow:'0px 1px 1px 1px #D0D0D0',borderRadius:'10px'}}>
                      <p>Average Salary</p>
                      <p>54000</p>
                    </div>
                  </div>
                  </div>
               </div>
          </div>
        </div>

  </div>
   
  )
}
