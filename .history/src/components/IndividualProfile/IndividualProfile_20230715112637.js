import React,{useState,useEffect} from 'react'
import './index.css'
  import Sidebar from '../Sidebar';
  import Files from './Files/files.js'
  import Topbar from '../Topbar';
  import KycProfile from './KycProfile/kycProfile.js';
  import CreditProfile from './CreditProfile/CreditProfile.js';
  import IncomeProfile from './IncomeProfile/IncomeProfile.js';
  import {
    Col,
    Table,
    Card,
    Form,
    Button,
    Input,
    CardBody,
    CardTitle,
    CardSubtitle,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Row,
    FormGroup,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
  } from 'reactstrap';
  import {useLocation,useNavigate} from 'react-router-dom';
  import {useSelector,useDispatch} from 'react-redux'
import {userActions} from '../../actions/user.actions';
import axiosInstance from '../../helpers/axios';
import 'bootstrap/dist/css/bootstrap.min.css';

  import activity from '../../assets/images/buttons/activity.png'

  import pencil from '../../assets/images/pencil.png'
  import userSelfie from '../../assets/images/icons/userselfie.png'
  import loanAgreement from '../../assets/images/buttons/loanAgreement.png'
  
 export default function IndividualProfile() {
  const location=useLocation();
  const navigate=useNavigate();
   
  const  curruserId=location?.state?.profileId
  const  currapplicationId=location?.state?.applicationId
  const user=localStorage.getItem("user");

  const activityState=useSelector((state)=>state?.ACTIVITY?.activityData)
  const digiLockerState=useSelector((state)=>state?.DIGILOCKER?.digiLockerData);
  const cibilState=useSelector((state)=>state?.CIBIL?.cibilData);
  const panState=useSelector((state)=>state?.PAN?.panStatus);
  const profileState=useSelector((state)=>state?.PROFILE?.profileDetails);
  const ckycState=useSelector((state)=>state?.CKYC?.ckycData);
  const bankSelfieState=useSelector((state)=>state?.BANKSELFIE?.bankSelfieData);
  const uploadedFileState=useSelector((state)=>state?.UPLOADEDFILES?.uploadedFilesDetails);

  const dispatch=useDispatch();
  
  const ckycUrl=`/ckyc/details?userId=${curruserId}`;
  const profileUrl=`/summary/user?id=${curruserId}`;
  const digilockerUrl=`/digilocker/details?applicationId=${currapplicationId}&userId=${curruserId}`;
  const panUrl=`/cibil/user-details?userId=${curruserId}`;
  const bankSelfieUrl=`/integrations/details?applicationId=${currapplicationId}&userId=${curruserId}`;
  const uploadedFileUrl=`/users/documents?userId=${curruserId}`
  const getActivityUrl=`/audit?applicationId=${currapplicationId}`
  //cibil
  const urlCIBIL = `${process.env.REACT_APP_BASE_URL}/cibil/report?userId=${curruserId}`;

  const [ckycDetails,setCkycDetails]=useState(null)
  const [activityData,setActivityDetails]=useState(null)
  const [cibilReport,setCibilReport]=useState(null)
  const [panDetails,setPanDetails]=useState(null)
  const [digiLockerDetails,setDigiLockerDetails]=useState(null)
  const [bankSelfieDetails,setBankSelfieDetails]=useState(null)
  const [uploadedFilesDetails,setUploadedFiles]=useState(null)
  const [profileDetails,setProfileDetails]=useState(null)

  useEffect(() => {
    
    
   
      if(profileState||uploadedFileState){
        console.log(profileState)
        setActivityDetails(activityState?.res?.data?.data)
        setDigiLockerDetails(digiLockerState?.res)
        setCkycDetails(ckycState?.res)
        setPanDetails(panState?.res);
        setCibilReport(cibilState?.res?.data?.data?.summary)
        setBankSelfieDetails(bankSelfieState?.res)
        setProfileDetails(profileState?.res?.data)
        setUploadedFiles(uploadedFileState?.res?.data?.data)
      }
      else{
        dispatch(userActions.getCkycDetails(ckycUrl,user));
        dispatch(userActions.getCibilDetails(urlCIBIL));
        dispatch(userActions.getPanStatus(panUrl));
        dispatch(userActions.getBankSelfieDetails(bankSelfieUrl,user));
        dispatch(userActions.getDigiLocker(digilockerUrl,user));
        dispatch(userActions.getProfileDetails(profileUrl));
        dispatch(userActions.getUploadedFiles(uploadedFileUrl,user));
        dispatch(userActions.getActivity(getActivityUrl,user));
      }
  
  }, [profileState,uploadedFileState])
  
  const [activeTab, setActiveTab] = useState('1');
   
  
 const toggle = (tab) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };

  const handleNavigateActivity=()=>{
    navigate('/status/profile/activity',{
      state:{
        activityDetails:activityData
      }
    })
  }


  return (
  <>
    {/* {ckycDetails?<p>Ckyc</p>:<p>No Ckyc</p>}
    {panDetails?<p>Ckyc</p>:<p>No Ckyc</p>}
    {digiLockerDetails?<p>Ckyc</p>:<p>No Ckyc</p>}
    {bankSelfieDetails?<p>Ckyc</p>:<p>No Ckyc</p>} */}
   <Card className="card" style={{width:"100%"}} >
    {
      console.log(profileDetails,"proooo")
    }
          <Sidebar/>
          <div style={{width:'75%', overflow:"auto",marginLeft:"auto",padding:"10px"}}>
          <Topbar/>
          <div className="profileHighlight">
            <div style={{display:'flex', margin:'1.5em'}}>
              <img style={{width:'6em',marginRight:'1em'}} src={userSelfie}/>
              <div style={{lineHeight:'15px',marginTop:'1em'}}>
                <p style={{fontSize:'25px',fontWeight:'600'}}>{profileDetails?.firstName} {profileDetails?.lastName}</p>
                <p style={{fontSize:'18px',color:'#667085',fontWeight:'bold'}}>Application Id : <span style={{color:'#D32028',textDecoration:'underline'}}>{currapplicationId}</span></p>
                <p>{profileDetails?.employerName}</p>
              </div>
            </div>
            <div style={{display:'flex',justifyContent: 'space-evenly',width:'20em',height:'3em',marginTop:'2em'}}>
              <img style={{cursor:'pointer'}} onClick={()=>handleNavigateActivity()} src={activity}/>
              <img style={{cursor:'pointer'}}  src={loanAgreement}/>
              
            </div>
          </div>
          <div style={{display:'block',backgroundColor:'#F8F8F8',border:'none',borderRadius:'1em',boxShadow:'0px 2px 2px 0px #C0C0C0'}}>
            <img src={pencil} title="Edit" style={{width:'1.5em',height:'1.5em',cursor:'pointer',margin:'1.5em',position: 'absolute',}}/>
            <div style={{display:'flex',justifyContent: 'space-around',paddingTop:'1em'}}>
                <div style={{display:'block',lineHeight:'18px'}}>
                <p style={{color:'#D32038'}}>Email Id</p>
                <p>{profileDetails?.email}</p>
              </div>
              <div style={{display:'block',lineHeight:'18px'}}>
                <p  style={{color:'#D32038'}}>Phone Number</p>
                <p>{profileDetails?.mobile}</p>
              </div>
              <div style={{display:'block',lineHeight:'18px'}}>
                <p  style={{color:'#D32038'}}>Ref Phone Number</p>
                <p>Have to implement</p>
              </div>
            </div>
            <div style={{display:'flex',justifyContent: 'space-around',marginTop:'2em',paddingBottom:'1em'}}>
              <div style={{display:'block',lineHeight:'10px'}}>
                <p  style={{color:'#D32038'}}>Date of Birth</p>
                <p>{profileDetails?.dateOfBirth}</p>
              </div>
              <div style={{display:'block',lineHeight:'18px'}}>
                <p  style={{color:'#D32038'}}>Permanent Address</p>
                <p>Not available</p>
              </div>
              <div style={{display:'block',lineHeight:'18px',width:'9em'}}>
                <p  style={{color:'#D32038'}}>Current Address</p>
                <p title={profileDetails?.currentAddress} style={{width:'9em'}}>{profileDetails?.currentAddress.length>30?profileDetails?.currentAddress.substr(0,40)+"...":profileDetails?.currentAddress}</p>
              </div>
            </div>
           
          </div>
          <div style={{paddingLeft: '1em',paddingRight:'1em',marginTop:'1em'}}>
           <Nav tabs>
              <NavItem style={{backgroundColor:activeTab === '1' ? '#fff2f2' : '#fff'}}>
                <NavLink
                
                  className={activeTab === '1' ? 'active bg-transparent' : 'cursor-pointer'}
                  onClick={() => {
                    toggle('1');
                  }}
                >
                <span  className={activeTab === '1' ? 'tabsHeadingClicked' : 'tabsHeading'}>Files</span>

                </NavLink>
              </NavItem>

              <NavItem style={{backgroundColor:activeTab === '2' ? '#fff2f2' : '#fff'}}>
                <NavLink
                  className={activeTab === '2' ? 'active bg-transparent' : 'cursor-pointer'}
                  onClick={() => {
                    
                    toggle('2');
                  }}>
                  <span  className={activeTab === '2' ? 'tabsHeadingClicked' : 'tabsHeading'}>KYC Profile</span>
                </NavLink>
              </NavItem>
              <NavItem style={{backgroundColor:activeTab === '3' ? '#fff2f2' : '#fff'}}>
                <NavLink
                  className={activeTab === '3' ? 'active bg-transparent' : 'cursor-pointer'}
                  onClick={() => {
                     
                    toggle('3');
                  }}
                >
                  <span  className={activeTab === '3' ? 'tabsHeadingClicked' : 'tabsHeading'}>Credit Profile</span>
                </NavLink>
              </NavItem>
              <NavItem style={{backgroundColor:activeTab === '4' ? '#fff2f2' : '#fff'}}>
                <NavLink
                  className={activeTab === '4' ? 'active bg-transparent' : 'cursor-pointer'}
                  onClick={() => {
                    toggle('4');
                  }}
                >
                  <span  className={activeTab === '4' ? 'tabsHeadingClicked' : 'tabsHeading'}>Income Profile</span>
                </NavLink>
              </NavItem>
             
          
             
            </Nav>
           
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
                <Row>
                  <Col  sm="12">
                    <Files fileList={uploadedFilesDetails}/>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
                <Row>
                  <Col sm="12" >
                   
                   <KycProfile kycDetails={ckycDetails} bankVerificationDetails={bankSelfieDetails} panDetails={panDetails} digilockerDetails={digiLockerDetails}/>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="3">
                <Row>
                  <Col sm="12">
                   <CreditProfile cibilReport={cibilReport}/>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="4">
                <Row style={{ width: 'auto' }}>
                  <Col sm="12">
                    <IncomeProfile/>
                  </Col>
                </Row>
              </TabPane>
          
          
             
            </TabContent>
             </div>
          </div>
  </Card>
  </>
  )
}
