//import * as Icon from 'react-feather';
import {
  Col,
  Table,
  Card,
  Form,
  Button,
  Input,
  CardBody,
  CardTitle,
  CardSubtitle,
  Row,
  FormGroup,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Tooltip
} from 'reactstrap';
import axios from 'axios';
import { useState, useEffect } from 'react';
import download from "../../../assets/images/download.png";
import Sidebar from '../../Sidebar';
import Topbar from '../../Topbar';
import Datetime from 'react-datetime';
import jwt_decode from 'jwt-decode'
import 'react-datetime/css/react-datetime.css';

import { useLocation,useNavigate } from 'react-router-dom';
import EditForm from '../EditForm/EditForm';
import MoveProcessingStatus from '../MoveProcessingStatus/MoveProcessingStatus';
import Sort from "../../../assets/images/sort.png";

//import { useUserAuth } from '../../views/auth/AuthContext.js';
import "./ProcessingTable.css"
import PaginationTable from '../PaginationTable';
//import '../../assets/scss/_variables.scss';
import Wait from '../../../assets/images/sand-clock.png';
import axiosInstance from '../../../helpers/axios';
import { useSelector, useDispatch } from "react-redux";

import {userActions} from '../../../actions/user.actions';

// eslint-disable-next-line import/extensions
//import ComponentCard from '../ComponentCard';

//require('moment/locale/fr');
//require('moment/locale/de');
//require('moment/locale/en-gb');

const ProcessingTable = () => {
  const [userDetails, setUserDetails] = useState([])
  const state = useSelector((state) => state);

  const dispatch = useDispatch();

  const ApplicationStatusEnum = {
    InSubmission: 'In Submission',
    InReview: 'In Review',
    InSanction: 'In Sanction',
    InDisbursement: 'In Disbursement',
    Disbursed: 'Disbursed',
    Rejected: 'Rejected',
    Dropped: 'Dropped',
    OnHold:'On Hold', ReadyToDisburse:'Ready to Disburse',
  };
  const location=useLocation();
  const [status, setStatus] = useState(ApplicationStatusEnum.InProcess);
  const user = localStorage.getItem('user');
  const decode=jwt_decode(user)
  //filter
  const [filterInstituteName, setFilterInstituteName] = useState('');
  const [filterDateFrom, setFilterDateFrom] = useState('null');
  const [filterDateTo, setFilterDateTo] = useState(null);
  const valid = (current) => {
    return current.isAfter(filterDateFrom);
  };
//tooltip
 const [agreementTooltipOpen, setAgreementTooltipOpen] = useState(false);
 const [mandateTooltipOpen, setMandateTooltipOpen] = useState(false);


  const copyAgreementToggle = () => {

    setAgreementTooltipOpen(!agreementTooltipOpen);
    navigator.clipboard.writeText(`
              Aadhaar Suffix:${info?info.aadhaar_suffix:null},
              Name: ${info?info.name:null},
              Postal Code: ${info?info.postal_code:null},
              Display Name: ${info?info.display_name:null},
              Name Validation:${info?info.name_validation_score:null},
              Created At:${docDetails?docDetails.createdAt:null},
              Done At:${docDetails?docDetails.updatedAt:null},
              Download Signed Agreement Document:${docDetails?docDetails.signedDocumentUrl:null} `)
  }
  const copyMandateToggle = () => {

    setMandateTooltipOpen(!mandateTooltipOpen);
   navigator.clipboard.writeText(`
              Account Number:${mandateDetails?mandateDetails.maskedAccountNumber:null},
              Account Type: ${mandateDetails?mandateDetails.accountType:null},
              Customer mobile:${mandateDetails?mandateDetails.customerMobile:null},
              IFSC Code: ${mandateDetails?mandateDetails.ifscCode:null},
              NPCI Txn ID:${mandateDetails?mandateDetails.npciTxnId:null},
              UMRN:${mandateDetails?mandateDetails.umrn:null},
              Mandate Mode:${mandateDetails?mandateDetails.mandateMode:null},
              Created At:${mandateDetails?mandateDetails.createdAt:null},
              Done At:${mandateDetails?mandateDetails.updatedAt:null},
               `)
  }
  //table
  const [tableData, setTableData] = useState([]);
  const [filterTableData, setFilterTableData] = useState([]);
  const [userProfileId, setUserProfileId] = useState(null);
  const [time, setTime] = useState('');
  const [dropDownOpen, setDropDownOpen] = useState(false);
  const [info,setInfo]=useState(null)
  const [docDetails,setDocDetails]=useState(null)
  //pagination
  //changepage
  const paginate = (pageNumber) => {
    setcurrentPage(pageNumber);
  };

  // eslint-disable-next-line no-unneeded-ternary 
  const [currentPage, setcurrentPage] = useState(1);
 
  const [rowsPerPage, setrowsPerPage] = useState(10);
  const [totalFetchRows, setTotalFetchRows] = useState(null);
  const [formStatusOpen, setStatusOpen] = useState(false);
  const [mandateStatusOpen, setMandateStatusOpen] = useState(false);
  const [mandateDetails, setMandateDetails] = useState(null);
   
 const closeAgreeCopy=()=>{
  setStatusOpen(!formStatusOpen)
  setAgreementTooltipOpen(agreementTooltipOpen?!agreementTooltipOpen:agreementTooltipOpen);
 }
 const closeMandateCopy=()=>{
  setMandateStatusOpen(!mandateStatusOpen)
  setMandateTooltipOpen(mandateTooltipOpen?!mandateTooltipOpen:mandateTooltipOpen);
 }
 //sorting
const [sort,setSort]=useState(-1)

  const urlUsers = `/admin/application/in-progress?perPage=${rowsPerPage}&pageNo=${currentPage}&sortOrder=${sort}`;

  const[toggleSort,setToggleSort]=useState(false);
 

  useEffect(() => {

    if(toggleSort) setSort(1)
    else setSort(-1) 

    dispatch(userActions.fetchInprogressUsers(urlUsers, user))

      
    console.log("store", state);
    setUserDetails(state?.Inprogress?.applicants?.res?.applicantDetails)
    console.log("User Details")
    setTableData(state?.Inprogress?.applicants?.res?.applicantDetails);
    setFilterTableData(state?.Inprogress?.applicants?.res?.applicantDetails);

        // setcurrentPage(res.data.data.pagination.currentPage);
        // setTotalFetchRows(res.data.data.pagination.totalRows);
        // setrowsPerPage(res.data.data.pagination.perPage);
  }, [toggleSort,urlUsers]);


  const handleDocDownload=(docUserId)=>{
    setStatusOpen(!formStatusOpen);
      const docUrl=`/agreement/downloadAgreement?userId=${docUserId}`
    axiosInstance.post(docUrl).then((resp)=>{
      if(resp.data.data!=undefined)
      {

        setInfo(JSON.parse(resp.data.data.info))
        setDocDetails(resp.data.data)
      }
    }
    )
  }

  const handleMandateDetails=(muserApp,muserId)=>{
    setMandateStatusOpen(!mandateStatusOpen);
    const mandateUrl=`/mandate/mandate-details?applicationId=${muserApp}&userId=${muserId}`
    axiosInstance.post(mandateUrl).then((resp)=>
    setMandateDetails(resp.data.data))
  }
  //datefilter
  const handleDateFilterApi = (startDate, endDate) => {
    if (startDate > endDate) alert('Check From Date!!!');
    else if (startDate === null || endDate === null) alert('Enter Date Range to Filter');
    else {
      const urlFilterUsers = `/admin/application/in-progress?fromDate=${startDate}&toDate=${endDate}&perPage=${rowsPerPage}&pageNo=${currentPage}&sortOrder=${sort}`;
      axiosInstance.get(urlFilterUsers,{
        headers:{
          Authorization:`Bearer ${user}`
        }
      }).then((res) => {
        setTableData(res.data.data.details);
        setFilterTableData(res.data.data.details);
        
         setcurrentPage(res.data.data.pagination.currentPage);

        setTotalFetchRows(res.data.data.pagination.totalRows);
        setrowsPerPage(res.data.data.pagination.perPage);
      });
    }
  };

  //clearfilter
  const handleClearFilter = () => {
    setFilterInstituteName('');
    axiosInstance.get(urlUsers).then((res) => {
     setTableData(res.data.data.details);
        setFilterTableData(res.data.data.details);
        
         setcurrentPage(res.data.data.pagination.currentPage);

        setTotalFetchRows(res.data.data.pagination.totalRows);
        setrowsPerPage(res.data.data.pagination.perPage);
    });
  };
  const navigate = useNavigate();

  const handleUser = (event) => {
    
    const Id = event.userId;
    
    const urlProfile = `/summary/user?id=${Id}&status=In Progress`;
    axiosInstance.get(urlProfile).then((res) => {
      

      navigate('/Status/processing/profile', {
        state: { id: 1, profileId: Id, userData: res.data.data,applicationId:event.applicationId },
      });
    });
  };

  const handleCoApp = (event,currTab) => {
    
    const Id = event.coapplicantId;
    
    const urlProfile = `$/summary/coapplicant?id=${Id}`;
    axiosInstance.get(urlProfile).then((res) => {
      

      navigate('/Status/processing/profile', {
        state: { id: 1, profileId: Id, userData: res.data.data,currTab },
      });
    });
  };

    //Go To Application Profile
   const handleApplicationProfile=(currApplicationId)=>{
    localStorage.setItem("appId",currApplicationId)
    navigate("/Status/appProfile")
    
  }

   const handleDownloadList=()=>{
    document.getElementById("downloadButton").disabled=true;
    document.getElementById("downloadButton").innerText="Wait..";

    const downloadTemplateUrl=`/excel/download`
    const downloadData={
        instituteId: decode.instituteId,
        status:status,
       
    }
    axiosInstance.post(downloadTemplateUrl,downloadData, {
      responseType:'arraybuffer',
      headers:{
        Authorization:`Bearer ${user}`

    }
  }).then((res)=>{
    setTimeout(() => {
    document.getElementById("downloadButton").disabled=false
    document.getElementById("downloadButton").innerText="Download List";

    }, 2000);
    const blob = new Blob([res.data], { type: "application/xlsx", });
    const url= window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href =url
    link.download = 'demo.xlsx';

    document.body.appendChild(link);

    link.click();

    document.body.removeChild(link);
  }
      
    
  ).catch((error)=>{
    document.getElementById("downloadButton").innerText="Error Downloading";
    document.getElementById("downloadButton").style.backgroundColor="red";
    setTimeout(() => {
    document.getElementById("downloadButton").disabled=false
    document.getElementById("downloadButton").innerText="Download List";
    document.getElementById("downloadButton").style=null;

    }, 3000);
  })
  }  

  

  

  if (tableData) {
    return (
      <Card className="card" style={{display:'flex', flexDirection:'row',width:"100%"}}>
        <Sidebar/>
        <div style={{width:'75%', overflow:"auto",marginLeft:"auto",padding:"10px"}}>
        <Topbar/>
        <CardBody>
          <CardTitle style={{fontFamily:"Inter-Medium",fontSize: '14px'}}tag="h4">In Process Applications Listing</CardTitle>
          <p style={{fontFamily:"Inter-Medium",marginBottom:"0px",fontSize: '14px'}}>Filters</p>
          <br />
          <Row>
            <div style={{ display: 'flex', flex: 'wrap' }}>
              <Col sm="12" md="10" lg="3">
                <div style={{ height: '23px', marginRight: '10px' }}>
                  <Input
                  className='instname'
                    id="instName"
                    type="text"
                    onChange={(event) => setFilterInstituteName(event.target.value)}
                    placeholder="Institute Name (Case Sensitive)"
                  />
                </div>
              </Col>
              <Col>
                <div style={{ height: '7px', marginRight: '10px' }}>
                  <Datetime
                    id="fromDate"
                    dateFormat="YYYY/MM/DD"
                    timeFormat={false}
                    onChange={(event) => setFilterDateFrom(event.format('YYYY/MM/DD'))}
                    locale="en-gb"
                    inputProps={{ placeholder: 'From Date' }}
                  />
                </div>
              </Col>
              <Col>
                <div style={{ height: '7px', marginRight: '10px' }}>
                  <Datetime
                    id="toDate"
                    dateFormat="YYYY/MM/DD"
                    isValidDate={valid}
                    timeFormat={false}
                    locale="en-gb"
                    onChange={(event) => setFilterDateTo(event.format('YYYY/MM/DD'))}
                    inputProps={{ placeholder: 'To Date' }}
                  />
                </div>
              </Col>

              <Col>
                <Button className='search-results'
                  onClick={() => handleDateFilterApi(filterDateFrom, filterDateTo)}
                  color="success"
                >
                  Search Results
                </Button>
              </Col>
              <Col className='export-button'id="downloadButton" onClick={() => handleDownloadList()}>
                <img className='export-icon' src={download} alt="download"/>
                 <Button className="export-text"  >
                    Export
                  </Button>
                </Col>
            </div>
          </Row>

      <Modal isOpen={formStatusOpen}>
        <ModalHeader>Agreement Details</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
           
            
             <Button color='info'  style={{marginBottom:'1em',marginRight:'1em'}}
              onClick={() => copyAgreementToggle() }
            >
              Copy
            </Button>
             <p>Co-App Det</p>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
        

          <Button onClick={() => closeAgreeCopy()}>Close Box</Button>
        </ModalFooter>
      </Modal>
      
       <Modal isOpen={mandateStatusOpen}>
        <ModalHeader>Mandate Details</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
            { mandateDetails?<>
         
             <Button color='info'  style={{marginBottom:'1em',marginRight:'1em'}}
              onClick={() => copyMandateToggle() }
            >
              Copy
            </Button>
               {mandateTooltipOpen&&<span style={{fontSize:'11px',backgroundColor:'black',color:'white',borderRadius:'5em',padding:'0.5em',position:'absolute',marginTop:'0.5em'}}>Copied to clipboard</span>}
            <p><span style={{fontWeight:'700'}}>Account Number: </span>{mandateDetails?mandateDetails.maskedAccountNumber:null}</p>
             <p><span style={{fontWeight:'700'}}>Account Type: </span>{mandateDetails?mandateDetails.accountType:null}</p>
             <p><span style={{fontWeight:'700'}}>customer mobile:</span> {mandateDetails?mandateDetails.customerMobile:null}</p>
             <p><span style={{fontWeight:'700'}}>ifscCode:</span> {mandateDetails?mandateDetails.ifscCode:null}</p>
            <p ><span style={{fontWeight:'700'}}>npci Tax ID:</span> {mandateDetails?mandateDetails.npciTxnId:null}</p>
            <p ><span style={{fontWeight:'700'}}>umrn:</span> {mandateDetails?mandateDetails.umrn:null}</p>
            <p ><span style={{fontWeight:'700'}}>Mandate Mode:</span> {mandateDetails?mandateDetails.mandateMode:null}</p>
             <p><span style={{fontWeight:'700'}}>Created At:</span> {mandateDetails?mandateDetails.createdAt:null}</p>
             <p><span style={{fontWeight:'700'}}>Done At:</span> {mandateDetails?mandateDetails.updatedAt:null}</p></>:<h3>Information is not available</h3>}
             
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
        

          <Button onClick={() => closeMandateCopy()}>Close Box</Button>
        </ModalFooter>
      </Modal>
          <div style={{ marginTop: '30px' }}>
            <Table style={{ flex: 'wrap' }} hover>
              <thead className='table-heading'>
                <tr>
                  <th className="thead-text" onClick={()=>setToggleSort(!toggleSort)} style={{ fontSize: '15px' }} id="appId">
                    Appl. Id<img style={{cursor:'pointer',width:'15px',marginLeft:'5px'}} src={Sort} alt="sort"/>
                  </th>
                  <th className="thead-text" id="appname">Appl. Name</th>
                  <th className="thead-text" id="insName">Institute Name</th>

                  <th className="thead-text" id="coAppStatus"style={{width:'10em'}}>Co-App Status</th>
                  <th className="thead-text" id="bankStatus" style={{width:'11.5em'}}>Bank Stmnt Status</th>
                  
                  <th className="thead-text" id="amt">Amount</th>
                 
                 
                 
                  { decode.role==="institute_user"||decode.role==="user"||decode.role==="co_lender"?null:<th id="actions" className="thead-text" >Actions</th>}
                </tr>
              </thead>
              {tableData.map((item) => {
                const filterInstitute = filterInstituteName;
                const currentInstituteName = item.instituteName || '';

                if (currentInstituteName.indexOf(filterInstitute) !== -1)
                  return (
                    <tbody key={item.applicationId}>
                      <tr style={{ lineHeight: '28px' }}>
                        {decode.role!=="institute_user"?<td >
                          <p style={{fontFamily:"Inter-Medium", fontSize: '13px'}}>{item.applicationId}</p>
                        </td>:<td  style={{ fontSize: '13px', fontWeight: '500', fontFamily:'Inter', width: '8em' ,color:'#101828',fontFamily:"Inter-Medium"}}>
                          {item.applicationId}
                        </td>
                        }
                        {decode.role==='institute_user'?<td
                          style={{
                            fontSize: '13px'
                            ,fontFamily:"Inter-Medium",
                            
                            width: '20em',
                          }}
                         
                        >
                           <p style={{fontFamily:"Inter-Medium",fontSize: '13px'}}>{item.name}</p>{' '}
                          {item.coapplicantName ? (
                            <p style={{fontFamily:"Inter-Medium",fontSize: '13px'}}>
                              {' '}
                             {item.coapplicantName} ( C )
                            </p>
                          ) : (
                            ''
                          )}
                        </td>:<td
                          style={{
                            fontSize: '13px',
                            color: '#101828',
                            cursor: 'pointer',fontFamily:"Inter-Medium",
                            width: '20em',
                          }}
                        >
                          <p style={{fontFamily:"Inter-Medium"}}onClick={() => handleUser(item)}>{item.name}</p>{' '}
                          {item.coapplicantName ? (
                            <p style={{fontFamily:"Inter-Medium"}}onClick={() => handleCoApp(item)}>
                              {' '}
                              {item.coapplicantName} ( C )
                            </p>
                          ) : (
                            ''
                          )}
                        </td>}
                        <td style={{ fontSize: '13px', width: '20em', color: '#667085' ,fontFamily:"Inter-Medium" }}>{item.instituteName}</td>
                        <td
                          style={{ fontSize: '13px', width: '9em', color: '#667085',fontFamily:"Inter-Medium"  }}
                          onClick={() => console.log('Co-App Status')}
                        >
                         <p style={{ cursor:'pointer', fontWeight:'600',fontSize: '13px', width: '7em', color: '#667085',fontFamily:"Inter-Medium" ,backgroundColor:`${item.coapplicantStatus==='Completed'?'lightGreen':`${item.coapplicantStatus==='Pending'?'#A8A8A8':`${item.coapplicantStatus==='Awaiting'?'aliceBlue':`${item.emandate==='Initiated'?'blue':''}`}`}`}`,textAlign:'center',borderRadius:'1em',border:'1px solid grey'}}> {item.coapplicantStatus}  {item.coapplicantStatus==="Awaiting"?<img style={{width:'18px'}} src={Wait} alt="wait"/>:null}</p>
                        </td>
                        <td
                          style={{fontSize: '13px',cursor:'pointer', width: '9em', color: '#667085',fontFamily:"Inter-Medium" }}
                          onClick={()=>console.log("Bank Statement Status")}
                        >
                          <p style={{fontWeight:'600', color: '#667085',fontFamily:"Inter-Medium" , backgroundColor:`${item.bankStatementStatus==='Completed'?'lightGreen':`${item.bankStatementStatus==='Initiated'?'#A8A8A8':`${item.bankStatementStatus==='Pending'?'#FFC72C':`${item.emandate==='Initiated'?'#007FFF':''}`}`}`}`,textAlign:'center',border:'1px solid grey',borderRadius:'1em',width:'8em'}}>{item.bankStatementStatus}</p>
                        </td>
                        
                        <td style={{ width: "7em" , color: '#667085',fontFamily:"Inter-Medium" }}>{item.courseFees}</td>
                        
                       
                       
                       { decode.role==="institute_user"||decode.role==="user"||decode.role==="co_lender"?null: <td style={{fontSize: '13px',display:'flex', width:'6em' ,lineHeight:'6em', color: '#667085' ,fontFamily:"Inter-Medium"}}>
                            <p ><EditForm user={item.userId} appId={item.applicationId}/></p>
                          <p style={{marginLeft:'1.5em',}}><MoveProcessingStatus  ApplicantId={item} /></p>
                        </td>}
                      </tr>
                    </tbody>
                  );
              })}
            </Table>
            {totalFetchRows ? (
              <p style={{ textAlign: 'center', fontWeight: 'bold' ,fontFamily:"Inter-Medium",fontSize: '13px'}}>
                Showing {currentPage * rowsPerPage + 1 - rowsPerPage}-{currentPage * rowsPerPage} 
                 records
              </p>
            ) : (
              <p style={{ textAlign: 'center', fontWeight: 'bold',fontFamily:"Inter-Medium",fontSize: '13px' }}>No Records</p>
            )}

            <PaginationTable
              startPage="1"
              rowsPerPage={rowsPerPage}
              totalRows={totalFetchRows}
              paginate={paginate}
            />
          </div>
        </CardBody>
        </div>
      </Card>
    );
  }
  return <div>....Loading</div>;
};

export default ProcessingTable;
