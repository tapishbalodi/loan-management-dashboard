import {
    Col,
    Table,
    Card,
    Form,
    Button,
    Input,
    CardBody,
    CardTitle,
    CardSubtitle,
    Row,
    FormGroup,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
  } from 'reactstrap';
  import Sidebar from '../../Sidebar';
  import Topbar from '../../Topbar';
  import axios from 'axios';
  import { useState, useEffect } from 'react';
  import Datetime from 'react-datetime';
  import 'react-datetime/css/react-datetime.css';
  import jwt_decode from 'jwt-decode';
  import { useLocation,useNavigate } from 'react-router-dom';
  import EditForm from '../EditForm/EditForm';
  import MoveStatus from '../MoveStatus/MoveStatus';
  import ResendConsent from '../ResendContent/ResendConsent';
  //import RequestBankStatement from './RequestBankStatement';
  //import { useUserAuth } from '../../views/auth/AuthContext.js';
  import Sort from "../../../assets/images/sort.png";
  import download from "../../../assets/images/download.png";
  import PaginationTable from '../PaginationTable';
  //import '../../assets/scss/_variables.scss';
  import './ProjectTable.css'
import axiosInstance from '../../../helpers/axios';
import { useSelector, useDispatch } from "react-redux";

import {userActions} from '../../../actions/user.actions';
  
  // eslint-disable-next-line import/extensions
  //import ComponentCard from '../ComponentCard';
  
  require('moment/locale/fr');
  require('moment/locale/de');
  require('moment/locale/en-gb');
  
  const ProjectTables = () => {

    const state = useSelector((state) => state?.Insubmission?.res);
    console.log("State", state)
    // const state = useMemo(() => (res), []);
  
    const [userDetails, setUserDetails] = useState(null)
  
    const dispatch = useDispatch();

    const user = localStorage.getItem('user');
    const decode=jwt_decode(user)
    const ApplicationStatusEnum = {
    InSubmission: 'In Submission',
    InReview: 'In Review',
    InSanction: 'In Sanction',
    InDisbursement: 'In Disbursement',
    PendingDisbursement: "Pending Disbursement",
    Disbursed: 'Disbursed',
    Rejected: 'Rejected',
    Dropped: 'Dropped',
    OnHold:'On Hold', ReadyToDisburse:'Ready to Disburse',
  };
    const location=useLocation();
    const [status, setStatus] = useState(ApplicationStatusEnum.InSubmission);
    console.log("status is ",status)
  
    //filter
    const [filterInstituteName, setFilterInstituteName] = useState('');
    const [filterDateFrom, setFilterDateFrom] = useState('null');
    const [filterDateTo, setFilterDateTo] = useState(null);
    const valid = (current) => {
      return current.isAfter(filterDateFrom);
    };
  
    //table
    const [tableData, setTableData] = useState([]);
    const [filterTableData, setFilterTableData] = useState([]);
    const [userProfileId, setUserProfileId] = useState(null);
    const [time, setTime] = useState('');
    const [dropDownOpen, setDropDownOpen] = useState(false);
    //pagination
    const [currentPage, setcurrentPage] = useState(localStorage.getItem("page")?localStorage.getItem("page"):1);
    const [rowsPerPage, setrowsPerPage] = useState(10);
    const [totalFetchRows, setTotalFetchRows] = useState(null);
   //sorting
    const [sort,setSort]=useState(-1)
   
    const urlUsers = `/admin/application?status=${status}&perPage=${rowsPerPage}&pageNo=${currentPage}&sortOrder=${sort}`;
  
  
    const[toggleSort,setToggleSort]=useState(false);
    
    
  
    
  
    useEffect(() => {

      if(toggleSort) setSort(1)
      else setSort(-1) 
  
      if(state){
        setUserDetails(state.applicantDetails)
        setTableData(state.applicantDetails);
        setFilterTableData(state.applicantDetails);
        setcurrentPage(state.pagination.currentPage);
        console.log("curentpageeee",currentPage)
        setTotalFetchRows(state.pagination.totalRows);
        setrowsPerPage(state.pagination.perPage);  
      }
      else{
        dispatch(userActions.fetchInsubmissionUsers(urlUsers, user))
  
      }
    
          
    }, [toggleSort,urlUsers, state]);
  
    //datefilter
    const handleDateFilterApi = (startDate, endDate) => {
      
      if (startDate > endDate) alert('Check From Date!!!');
      else if (startDate === null || endDate === null) alert('Enter Date Range to Filter');
      else {
        const urlFilterUsers = `/admin/application?status=${status}&fromDate=${startDate}&toDate=${endDate}&perPage=${rowsPerPage}&pageNo=${currentPage}&sortOrder=${sort}`;
        axiosInstance.get(urlFilterUsers,{
          headers:{
            Authorization:`Bearer ${user}`
          }
        }).then((res) => {
          setTableData(res.data.data.applicantDetails);
          setFilterTableData(res.data.data.applicantDetails);
          setcurrentPage(res.data.data.pagination.currentPage);
  
          setTotalFetchRows(res.data.data.pagination.totalRows);
          setrowsPerPage(res.data.data.pagination.perPage);
        });
      }
    };
  
    //clearfilter
    const handleClearFilter = () => {
      setFilterInstituteName('');
      axiosInstance.get(urlUsers).then((res) => {
        setTableData(res.data.data.applicantDetails);
        setFilterTableData(res.data.data.applicantDetails);
        setcurrentPage(res.data.data.pagination.currentPage);
        setTotalFetchRows(res.data.data.pagination.totalRows);
        setrowsPerPage(res.data.data.pagination.perPage);
      });
    };
  
    const navigate = useNavigate();
  
    const handleUser = (event) => {
      
      const Id = event.userId;
      
      const urlProfile = `/summary/user?id=${Id}&status=In Submission`;
      axiosInstance.get(urlProfile).then((res) => {
        
        navigate('/Status/submission/profile', {
          state: { id: 1, profileId: Id, userData: res.data.data,applicationId:event.applicationId },
        });
      });
    };
    const handleCoApp = (event) => {
      
      const Id = event.coapplicantId;
      
      const urlProfile = `/summary/coapplicant?id=${Id}`;
      axiosInstance.get(urlProfile).then((res) => {
        
        navigate('/Status/submission/profile', {
          state: { id: 1, profileId: Id, userData: res.data.data,applicationId:event.applicationId },
        });
      });
    };
  
  
     //Go To Application Profile
     const handleApplicationProfile=(currApplicationId)=>{
      localStorage.setItem("appId",currApplicationId)
      navigate("/Status/appProfile")
      
    }
    //changepage
    const paginate = (pageNumber) => {
      setcurrentPage(pageNumber);
      dispatch(userActions.fetchInsubmissionUsers(urlUsers, user))

    };
  
  
     const handleDownloadList=()=>{
      document.getElementById("downloadButton").disabled=true;
      document.getElementById("downloadButton").innerText="Wait..";
  
      const downloadTemplateUrl=`/excel/download`
      const downloadData={
          instituteId: decode.instituteId,
          status:status,
         
      }
      axiosInstance.post(downloadTemplateUrl,downloadData, {
        responseType:'arraybuffer',
        headers:{
          Authorization:`Bearer ${user}`
  
      }
    }).then((res)=>{
      setTimeout(() => {
      document.getElementById("downloadButton").disabled=false
      document.getElementById("downloadButton").innerText="Export";
  
      }, 2000);
      const blob = new Blob([res.data], { type: "application/xlsx", });
      const url= window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href =url
      link.download = 'demo.xlsx';
  
      document.body.appendChild(link);
  
      link.click();
  
      document.body.removeChild(link);
    }
        
      
    ).catch((error)=>{
      document.getElementById("downloadButton").innerText="Error Downloading";
      document.getElementById("downloadButton").style.backgroundColor="red";
      setTimeout(() => {
      document.getElementById("downloadButton").disabled=false
      document.getElementById("downloadButton").innerText="Download List";
      document.getElementById("downloadButton").style=null;
  
      }, 3000);
    })
    }  
  
  
  //resend consent
  const [resendModal,setResendModal]=useState(false)
  const urlResendConsent=`/end-user/submit/resend-consent`
  const handleResendConsent=(item)=>{
    const data={
      userId:item.userId,
      mobile:item.mobile
    }
    axiosInstance.post(urlResendConsent,data,{
      headers:{
        Authorization:`Bearer ${user}`
      }
    })
    .then((res)=>alert(`Resend Consent ${res.data.message}`))
    .catch((err)=>alert(err));
    
  }
  
   
  console.log("tabledata is ", tableData)
    if (tableData) {
     
      return (
        <Card className='card' style={{display:'flex', flexDirection:'row',width:"100%"}}>
          
          <Sidebar/>
          
          <div style={{width:'75%', overflow:"auto",marginLeft:"auto",padding:"10px"}}>
            <Topbar/>
          
          <CardBody className='card-body'> 
          <Modal isOpen={resendModal}>
              <ModalHeader>Resend Consent</ModalHeader>
            <ModalBody>
              <p>On Clicking <strong> Resend Consent </strong> an SMS will be sent to the Applicant</p>
              </ModalBody>
              <ModalFooter>
              <Button  onClick={()=>setResendModal(!resendModal)} type="button">Close Box</Button>
              <Button color="primary" onClick={()=>handleResendConsent()} type="button">Resend Consent</Button>
              </ModalFooter>
          </Modal>
           
            <CardTitle style={{fontFamily:"Inter-Medium", fontSize:"14px"}} tag="h5">Customer Listing</CardTitle>
            <p style={{fontFamily:"Inter-Medium", fontSize:"14px",marginBottom:"0px"}}>Filters</p>
            <br />
            <Row>
              <div style={{ display: 'flex', flex: 'wrap' }}>
                <Col sm="12" md="10" lg="3">
                  <div style={{ height: '23px', marginRight: '10px' }}>
                    <Input
                      className='instname'
                      id="instName"
                      type="text"
                      onChange={(event) => setFilterInstituteName(event.target.value)}
                      placeholder="Institute Name (Case Sensitive)"
                    />
                  </div>
                </Col>
                <Col>
                  <div style={{ height: '7px', marginRight: '10px' }} >
                    <Datetime
                    
                      id="fromDate"
                      dateFormat="YYYY/MM/DD"
                      timeFormat={false}
                      onChange={(event) => setFilterDateFrom(event.format('YYYY/MM/DD'))}
                      locale="en-gb"
                      inputProps={{ placeholder: 'From Date' }}
      />
      </div>
                </Col>
                <Col >
                  <div style={{ height: '7px', marginRight: '10px', }}>
                    <Datetime
                      id="toDate"
                      dateFormat="YYYY/MM/DD"
                      isValidDate={valid}
                      timeFormat={false}
                      locale="en-gb"
                      onChange={(event) => setFilterDateTo(event.format('YYYY/MM/DD'))}
                      inputProps={{ placeholder: 'To Date' }}
                    />
                  </div>
                </Col>
                <Col>
                <Button className='search-results'
                  onClick={() => handleDateFilterApi(filterDateFrom, filterDateTo)}
                  color="success"
                >
                  Search Results
                </Button>
              </Col>
                <Col className='export-button'id="downloadButton" onClick={() => handleDownloadList()}>
                <img className='export-icon' src={download} alt="download"/>
                 <Button className="export-text"  >
                    Export
                  </Button>
                </Col>
              </div>
            </Row>
            <div style={{ marginTop: '30px' }}>
              <div className='table1'>
              <Table>
                <thead className='table-heading' >
                  <tr>
                    <th className="thead-text" onClick={()=>setToggleSort(!toggleSort)} style={{ fontSize: '14px',cursor:'pointer' }} id="appId">
                      Appl. Id<img style={{cursor:'pointer',width:'14px',marginLeft:'5px,fontFamily:"Inter-Medium"'}} src={Sort} alt="sort"/>
                    </th>
                    <th className="thead-text" style={{ fontSize: '14px',fontFamily:"Inter-Medium" }} id="appname">
                      Appl. Name
                    </th>
                    <th className="thead-text" style={{ fontSize: '14px',fontFamily:"Inter-Medium" }} id="insName">
                      Institute Name
                    </th>
                    <th className="thead-text" style={{ fontSize: '14px' ,fontFamily:"Inter-Medium"}} id="coursename">
                      Course
                    </th>
                    <th className="thead-text" onClick={()=>setToggleSort(!toggleSort)} style={{ fontSize: '14px',cursor:'pointer',fontFamily:"Inter-Medium" }} id="appfromdate">
                       Date<img style={{cursor:'pointer',width:'14px',marginLeft:'2px',fontFamily:"Inter-Medium"}} src={Sort} alt="sort"/>
                    </th>
                    <th className="thead-text" style={{ fontSize: '14px' ,textAlign:'center',fontFamily:"Inter-Medium"}} id="cra">
                      CRA
                    </th>
                    <th className="thead-text" style={{ fontSize: '14px',fontFamily:"Inter-Medium" }} id="amt">
                      Amount
                    </th>
                   { decode.role==="institute_user"||decode.role==="user"?null:<th style={{ fontSize: '14px',fontFamily:"Inter-Medium" }} id="actions" className="thead-text">
                      Actions
                    </th>}
                  </tr>
                </thead>
                {tableData.map((item) => {
                  const filterInstitute = filterInstituteName;
                  const currentInstituteName = item.instituteName || '';
                  const currentApplicationDate = item.applicationDate.substr(0, 10) || '';
  
                  if (currentInstituteName.indexOf(filterInstitute) !== -1)
                    return (
                      <tbody key={item.applicationId}>
                        <tr style={{ lineHeight: '28px'}}>
                          {decode.role!=="institute_user"?<td>
                           <p onClick={()=>handleApplicationProfile(item.applicationId)} style={{cursor:'pointer',fontSize: '14px', fontWeight: '500', fontFamily:'Inter-Medium', width: '8em' ,color:'#101828' }}> {item.applicationId}</p>
                          </td>:<td style={{ fontSize: '13px', fontWeight: '400', width: '12em',fontFamily:"Inter-Medium" }}>
                            {item.applicationId}
                          </td>
                          }
                           {decode.role==='institute_user'?<td
                            style={{
                              fontSize: '13px',
                              fontFamily:"Inter-Medium",
                              width: '17em',
                            }}
                           
                          >
                             <p style={{fontSize: '13px',
                              fontFamily:"Inter-Medium"}}>{item.name}</p>{' '}
                            {item.coapplicantName ? (
                              <p style={{fontSize: '13px',
                              fontFamily:"Inter-Medium"}}>
                                {' '}
                               {item.coapplicantName} ( C )
                              </p>
                            ) : (
                              ''
                            )}
                          </td>:<td
                            style={{
                              fontSize: '13px',
                              color: '#101828',
                              cursor: 'pointer',
                              width: '18em',
                              fontFamily:"Inter-Medium",
                            }}
                          >
                            <p style={{fontSize: '13px',
                              fontFamily:"Inter-Medium"}} onClick={() => handleUser(item)}>{item.name}</p>{' '}
                            {item.coapplicantName ? (
                              <p style={{fontSize: '13px',
                              fontFamily:"Inter-Medium"}} onClick={() => handleCoApp(item)}>
                                {' '}
                               {item.coapplicantName} ( C )
                              </p>
                            ) : (
                              ''
                            )}
                          </td>}
                          <td style={{ fontSize: '13px', fontWeight: '400', width: '15em' , color: '#667085',fontFamily:"Inter-Medium"}}>
                            {item.instituteName}
                          </td>
                          <td style={{ width: '18em', fontWeight: '400', fontSize: '13px', color: '#667085',fontFamily:"Inter-Medium" }}>
                            {item.courseName}
                          </td>
                          <td
                            title={item.applicationDate.substr(10, 14)}
                            style={{ width: '8em', fontWeight: '400', fontSize: '13px', color: '#667085',fontFamily:"Inter-Medium" }}
                          >
                            {item.applicationDate.substr(0, 10)}
                          </td>
                          <td title={item.CRA} style={{ fontSize: '13px', fontWeight: '500',textAlign:'center', width: 'auto' ,fontFamily:"Inter-Medium"}}>
                            {item.CRA?item.CRA[0]+item.CRA.toUpperCase().substr(item.CRA.indexOf(" "),item.CRA.indexOf("")+2):null}
                          </td>
                          <td style={{ fontSize: '13px', fontWeight: '400', width: '8em',fontFamily:"Inter-Medium" }}>
                            {item.courseFees}
                          </td>
                         { decode.role==="institute_user"||decode.role==="user"?null:<td style={{ display: 'flex', lineHeight: '6em' }}>
                            <div style={{ position: 'absolute' }}>
                              <EditForm user={item.userId} appId={item.applicationId} />
                            </div>
                            <div style={{ position: 'relative', marginLeft:"25px" }}>
                              <MoveStatus ApplicantId={item} />
                            </div>
                            <div title="Resend Consent" style={{ position: 'relative',marginLeft:"10px" }}>
                              <ResendConsent userInfo={item}/>
                            </div>
                          
                          </td>}
                        </tr>
                      </tbody>
                    );
                })}
              </Table>
              </div>
              {totalFetchRows ? (
                <p style={{ textAlign: 'center', fontWeight: 'bold',fontFamily:"Inter-Medium",fontSize: '13px' }}>
                  Showing {currentPage * rowsPerPage + 1 - rowsPerPage}-{currentPage * rowsPerPage}{' '}
                  records
                </p>
              ) : (
                <p style={{ textAlign: 'center', fontWeight: 'bold', fontFamily:"Inter-Medium",fontSize: '13px' }}>No Records</p>
              )}
              <PaginationTable
                startPage="1"
                rowsPerPage={rowsPerPage}
                totalRows={totalFetchRows}
                paginate={paginate}
              />
            </div>
          </CardBody>
          </div>
        </Card>
      );
    }
    return <div>....Loading</div>;
  };
  
  export default ProjectTables;