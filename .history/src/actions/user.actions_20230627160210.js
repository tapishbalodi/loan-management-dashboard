import { userConstants } from "./../constants/user.constants";
import {userService} from "../services/user.services";

export const userActions = {
    login,
    fetchInprogressUsers
    
  };

  function login(emailId, password, navigate) {

    return (dispatch) => {
        dispatch(request({ emailId }))

        userService.login(emailId, password).then(
            (user) => {

              console.log(user, "inside login function")
              dispatch(success(user));
              navigate('/dashboard')
            //   history.push("/");
            },
            (error) => {
              dispatch(failure(error.toString()));
            }
        )
    }

    function request(user) {
        return { type: userConstants.LOGIN_REQUEST, user };
      }
      function success(user) {
        return { type: userConstants.LOGIN_SUCCESS, user };
      }
      function failure(error) {
        return { type: userConstants.LOGIN_FAILURE, error };
      }
}

function fetchInprogressUsers(url , user) {

  return (dispatch) => {
      dispatch(request({ user }))

      userService.fetchInprogressUsers(url, user).then(
          (res) => {
            console.log("Fooooooooo", res)
            dispatch(success(res?.applicantDetails));
            // navigate('/dashboard')
          //   history.push("/");
          },
          (error) => {
            dispatch(failure(error.toString()));
          }
      )
  }

  function request(user) {
      return { type: userConstants.INPROGRESS_REQUEST, user };
    }
    function success(res) {
      return { type: userConstants.INPROGRESS_SUCCESS, res };
    }
    function failure(error) {
      return { type: userConstants.INPROGRESS_FAILURE, error };
    }
}