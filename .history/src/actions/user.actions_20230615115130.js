import { userConstants } from "./../constants/user.constants";
import {userService} from "../services/user.services";

export const userActions = {
    login
    
  };

  function login(emailId, password, navigate) {

    return (dispatch) => {
        dispatch(request({ emailId }))

        userService.login(emailId, password).then(
            (user) => {
              dispatch(success(user));
              navigate('/dashboard')
            },
            (error) => {
              dispatch(failure(error.toString()));
            }
        )
    }

    function request(user) {
        return { type: userConstants.LOGIN_REQUEST, user };
      }
      function success(user) {
        return { type: userConstants.LOGIN_SUCCESS, user };
      }
      function failure(error) {
        return { type: userConstants.LOGIN_FAILURE, error };
      }
}