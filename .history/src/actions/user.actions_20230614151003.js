import { userConstants } from "./../constants/user.constants";


export const userActions = {
    login
    
  };

  function login(emailId, password) {
    return (dispatch) => {
        dispatch(request({ emailId }))

        userService.login(emailId, password).then(
            (user) => {
              console.log("user is", user)
              dispatch(success(user));
            //   history.push("/");
            },
            (error) => {
              dispatch(failure(error.toString()));
            }
        )
    }

    function request(user) {
        return { type: userConstants.LOGIN_REQUEST, user };
      }
      function success(user) {
        return { type: userConstants.LOGIN_SUCCESS, user };
      }
      function failure(error) {
        return { type: userConstants.LOGIN_FAILURE, error };
      }
}