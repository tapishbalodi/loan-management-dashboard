/* eslint-disable no-unused-vars */
import React  from 'react';
import { Routes, Route } from "react-router-dom"
import {Provider} from "react-redux"
import Login from "./components/login"
import {store} from "./helpers/store"
import PrivateRoutes from './PrivateRoutes';
import Dashboard from './components/dashboard';
import ProjectTables from './components/status/ProjectTable/ProjectTables';
import ProcessingTable from './components/status/ProcessingTable/ProcessingTable';
import SanctionTable from './components/status/SanctionTable/SanctionTable';
import RejectedTable  from './components/status/RejectedTable/RejectedTable';
import HoldTable from './components/status/OnHoldTable/OnHoldTable';
import DroppedTable from './components/status/DroppedTable/DroppedTable';
import DisbursedTable from './components/status/DisbursedTable/DisbursedTable';
import Activity from './components/Activity/Activity';
import ReadyToDisburseTable from './components/status/ReadyToDisburse/ReadyToDisburse';
import ReviewTable from './components/status/ReviewTable/ReviewTable';
import SearchTable from './components/UserSearch/UserSearch';
import Sample from './components/status/Sample';

import IndividualProfile from './components/IndividualProfile/IndividualProfile'
import ApplicationProfile from './components/status/ApplicationProfile';
import PendingTable from './components/status/PendingTable/PendingTable'
//import ResendConsent from './components/status/ResendConsent';
// import { AuthContextProvider } from './AuthContext';
import './App.css'



function App() {

  return (
    // <AuthContextProvider>
    <Provider store={store}>
    <div className="App" style={{height:"100%"}}>
    
        
     <Routes>
        <Route element={<PrivateRoutes />}>
            <Route path="/dashboard" element={<Dashboard/>} />
            <Route path="/status/Submission" element={<ProjectTables/>} />
            <Route path="/status/Process" element={<ProcessingTable/>} />
            <Route path="/status/Sanction" element={<SanctionTable/>} />
            <Route path="/status/Rejected" element={<RejectedTable/>} />
            <Route path="/status/Hold" element={<HoldTable/>} />
            <Route path="/status/Dropped" element={<DroppedTable/>} />
            <Route path="/status/Disbursed" element={<DisbursedTable/>} />
            <Route path="/status/Disbursement" element={<ReadyToDisburseTable/>} />
            <Route path="/status/Pending" element={<PendingTable/>} />
            <Route path="/status/Review" element={<ReviewTable/>} />
            <Route path="/usersearch" element={<SearchTable/>} />
            <Route path="/Status/appProfile" element={<ApplicationProfile/>} />
            {/* <Route path="/studentList1" element={<Table/>} /> */}
            <Route path="/studentList1" element={<ApplicationProfile/>} />
            <Route path="/status/Disbursed/profile" element={<IndividualProfile/>}/>
            <Route path="/status/profile/activity" element={<Activity/>}/>
        </Route>
        <Route path="/" element={ <Login /> }  />
  
  </Routes>
  </div>
 {/* </AuthContextProvider> */}
  </Provider>
  );
}

export default App;