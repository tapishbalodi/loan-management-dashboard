export function authHeader() {

    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.data) {
        return { 'Authorization': 'Bearer ' + user?.data };
    } else {
        return {};
    }
}