import { combineReducers } from "redux";
import { authentication } from "./authentication.reducer";
import {Inprogress} from "./inprogress.user.reducer";


const rootReducer = combineReducers({

  authentication: authentication,
  Inprogress : Inprogress
 
});

export default rootReducer;