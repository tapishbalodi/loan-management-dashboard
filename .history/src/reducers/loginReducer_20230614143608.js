import { LOGIN_SUCCESS, LOGIN_FAILURE } from "../actionTypes";

const initialState = {
  numOfItems: 0,
};

 const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        numOfItems: state.numOfItems + 1,
      };

    case LOGIN_FAILURE:
      return {
        ...state,
        numOfItems: state.numOfItems - 1,
      };
    default:
      return state;
  }
};

export default loginReducer