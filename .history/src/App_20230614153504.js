/* eslint-disable no-unused-vars */
import React  from 'react';
import { Routes, Route } from "react-router-dom"

import Login from "./components/login"
// import PrivateRoutes from './views/PrivateRoutes';

// import { AuthContextProvider } from './AuthContext';
 
 
import './App.css'


function App() {

  return (
    // <AuthContextProvider>
    <div className="App" style={{height:"100%"}}>
    
        
     <Routes>
        {/* <Route element={<PrivateRoutes />}>

        </Route> */}
        <Route path="/login" element={ <Login /> }  />
  
  </Routes>
  </div>
// </AuthContextProvider>
  );
}

export default App;