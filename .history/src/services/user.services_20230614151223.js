import axiosInstance from '../helpers/axios'

export const userService = {
    login,
    logout
  };


async function login(emailId, password) {
  
    const payLoad = {
        emailId : emailId,
        password : password
    }
    
    const user =  axiosInstance.post(`/auth/verify`,payLoad)
    .then((res) => {
        console.log("Res", res)
        return res
    })
    .catch((err) => {
        console.log("Err", err)
        return err
    });

    localStorage.setItem("user", user);

    return user;
}


  function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("user");

  }