import {axiosInstance} from '../helpers/axios'
export const userService = {
    login
  };


async function login(emailId, password) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Headers": "*",
      email: email,
    },
    body: JSON.stringify({ email, password }),
  };

  const response = await fetch(
    `http://localhost:8080/api/auth/login`,
    requestOptions
  );
  const user = await handleResponse(response);
  // store user details and jwt token in local storage to keep user logged in between page refreshes
  localStorage.setItem("user", JSON.stringify(user));
  return user;
}