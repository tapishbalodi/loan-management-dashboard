import axiosInstance from '../helpers/axios'

export const userService = {
    login,
    logout,
    fetchInprogressUsers
  };


async function login(emailId, password) {
  
    const payLoad = {
        emailId : emailId,
        password : password
    }
    
    const user =  axiosInstance.post(`/auth/verify`,payLoad)
    .then((res) => {
        console.log("Res", res?.data)
        localStorage.setItem("user", res?.data?.data);
        return res?.data?.data
    })
    .catch((err) => {
        console.log("Err", err)
        return err
    });
    

    return user;
}


  function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("user");

  }


  async function fetchInprogressUsers(url, user) {
  
    
    
    axiosInstance.post(url,{
      headers : {
        Authorization: `Bearer ${user}`,
        'Content-type': 'application/json',
        
      }
    })
    .then((res) => {
        console.log("Res", res?.data)
        return res?.data?.data
    })
    .catch((err) => {
        console.log("Err", err)
        return err
    });
    

    return user;
}

  
