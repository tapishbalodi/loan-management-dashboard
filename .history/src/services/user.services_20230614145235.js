import axiosInstance from '../helpers/axios'

export const userService = {
    login
  };


async function login(emailId, password) {
  

  const response = await axiosInstance.post(
    `/auth/verify`,
    requestOptions
  );
  const user = await handleResponse(response);
  // store user details and jwt token in local storage to keep user logged in between page refreshes
  localStorage.setItem("user", JSON.stringify(user));
  return user;
}