/* eslint-disable no-unused-vars */
import React  from 'react';
import { Routes, Route } from "react-router-dom"

import LoginForm from "./views/loginForm"

// import PrivateRoutes from './views/PrivateRoutes';

// import { AuthContextProvider } from './AuthContext';
 
 
import './App.css'


function App() {

  return (
    // <AuthContextProvider>
    <div className="App" style={{height:"100%"}}>
    
        
     <Routes>
        {/* <Route element={<PrivateRoutes />}>

        </Route> */}
        <Route path="/login" element={ <LoginForm /> }  />
  
  </Routes>
  </div>
// </AuthContextProvider>
  );
}

export default App;