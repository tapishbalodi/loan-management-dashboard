import { createStore } from "redux";
import { loginReducer } from "./reducers/authentication.reducer";

const store = createStore(loginReducer);

export default store;