import * as React from 'react';
import Sidebar from "../Sidebar";
import Topbar from "../Topbar";
import {
    Chart as ChartJS,
    LinearScale,
    ArcElement,
    CategoryScale,
    BarElement,
    PointElement,
    LineElement,
    Legend,
    Tooltip,
    LineController,
    BarController,
  } from "chart.js";
  import { Chart, Bar, Doughnut,Line } from "react-chartjs-2";
  import { useState, useEffect  } from 'react';
 

const DemandReports = () =>{
    const options = {
        responsive: true,
        plugins: {
         
         
        },
      };
      const data1 = {
        labels:['October 2023','November 2023','December 2023','January 2024','February 2024','March 2024'],
        datasets: [
          {
            label: 'Demand',
            data: ["1234","2234","3334","4434","5534","6634"],
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          },
          {
            label: 'Collection',
            data: ["1130","1230","2330","3430","4530","5630"],
            backgroundColor: 'rgba(53, 162, 235, 0.5)',
          },
         
        ],
      };
      const [isOpen,setIsOpen]=useState(true)

    return(
        <div style={{display:'flex',width:"100%",backgroundColor:'white',minHeight:'100vh'}}>
        <Sidebar isOpenSidebar={isOpen} handler={setIsOpen}/>
        <div style={{width:isOpen?'80%':'100%', overflow:"auto",marginLeft:isOpen?"20%":'0%',padding:'10px'}}>
        <Topbar/>
        <div style={{backgroundColor:'#F8F8F8',borderRadius:'10px',margin:'15px',padding:'20px',display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
        <p style={{fontFamily:'Inter-Medium',color:'#667085',fontSize:'25px'}}>Demand Vs Collection</p>
        <Bar redraw={true} options={options} data={data1}/>
        </div>
        <div className='tables' style={{ marginTop: '20px' }}>
        <table>
            <thead className='table-heading'>
                <tr>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'12%',fontFamily:"Inter-Medium",borderTopLeftRadius:'8px' ,paddingLeft:'5px'}}>Collection Month</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'5%',fontFamily:"Inter-Medium"}}>Count</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium"}}>Arrears Amt.</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium"}}>Instalment Amt.</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium"}}>TotalDemand</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'15%',fontFamily:"Inter-Medium"}}>Instalment Rcvd.</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium"}}>Arrears Rcvd.</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium"}}>Advance Rcvd.</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium"}}>Preclose Rcvd.</th>
                    <th style={{  fontSize: '0.9vw',cursor:'pointer',width:'10%',fontFamily:"Inter-Medium",borderTopRightRadius:'8px',paddingRight:'5px' }}>TotalCollection</th>
                </tr>
            </thead>
            <tbody className='table-body'>
                <tr className="table-row">
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>FEB-2024</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>1437</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹2078734</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹9257990</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹11336724</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹8899132</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹265741</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹1180927</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹239922</td>
                    <td style={{  fontSize: '0.9vw',cursor:'pointer',width:'10em',fontFamily:"Inter-Medium",color:'#667085'}}>₹10585722</td>
                </tr>
            </tbody>
        </table>
        </div>
        </div>
       
        </div>
    )
}

export default DemandReports;