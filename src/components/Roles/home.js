import React,{useEffect} from 'react'
import axiosInstance from '../../helpers/axios'
import jwt_decode from 'jwt-decode'
import EditRole from './editRole'
import DeleteRole from './deleteRole'

export default function Home() {

  const user=sessionStorage.getItem('user')
  const decode=jwt_decode(user)
  const [users,setUsers]=React.useState(null)

  useEffect(() => {
    if(decode?.role!=="institute_user"&&decode?.role!=="institute_admin")
    {
      axiosInstance.get('/admin/users',{
        headers:{
          Authorization:  `Bearer ${user}`
        }
      })
      .then((res)=>{
  
        setUsers(res?.data?.data)
      })
    }
    else
    {
         axiosInstance.get(`/admin/institute-users?instituteId=${decode?.instituteId}`)
      .then((res)=>{
  
        setUsers(res?.data?.data)
      })
    }
  
    
  }, [])
  

  return (
    <div style={{display:'block',justifyContent:'center',height:'100%',boxShadow:'0px 0px 6px 0px rgb(0,0.5,0.8,0.3)',margin:'5%',borderRadius:'10px',marginLeft:'10%'}}>
      <div  style={{display:'flex',padding:'1em',justifyContent:'space-between',backgroundColor:'#F8F8F8',}}>
        <div style={{display:'flex',justifyContent:'center'}}>
          <p style={{fontFamily:'Inter',fontWeight:'bold',textAlign:'left',width:'10em'}}>Name</p>
          
        </div>
        <div  style={{display:'flex',justifyContent:'center'}}>
          <p style={{fontFamily:'Inter',fontWeight:'bold',textAlign:'center',width:'8em'}}>Phone</p>
          
        </div>
        <div  style={{display:'flex',justifyContent:'center'}}>
          <p style={{fontFamily:'Inter',fontWeight:'bold',textAlign:'center',width:'8em'}}>Role</p>
          
        </div>
        <div  style={{display:'flex',justifyContent:'center'}}>
          <p style={{fontFamily:'Inter',fontWeight:'bold',textAlign:'center',width:'13em'}}>Email</p>
         
        </div>
        {decode?.role==="admin"||decode?.role==="institute_admin"?<div  style={{display:'flex',justifyContent:'center'}}>
          <p style={{fontFamily:'Inter',fontWeight:'bold',textAlign:'right',}}>Actions</p>
         
        </div>:null}
      </div>
     {users?.map((item)=>{
      return(

         <div  style={{display:'flex',padding:'1em',justifyContent:'space-between',borderBottom:'1px solid #DbDbDb'}}>
        <div style={{display:'flex',justifyContent:'center',}}>
          <p style={{fontFamily:'Inter',textAlign:'left',width:'10em'}}>{item.name}</p>
          
        </div>
        <div  style={{display:'flex',justifyContent:'center',}}>
          <p style={{fontFamily:'Inter',textAlign:'center',width:'8em'}}>{item.mobile}</p>
          
        </div>
        <div  style={{display:'flex',justifyContent:'center',}}>
          <p style={{fontFamily:'Inter',textAlign:'center',width:'8em'}}>{item.role}</p>
          
        </div>
        <div  style={{display:'flex',justifyContent:'center',}}>
          <p style={{fontFamily:'Inter',textAlign:'right',width:'13em'}}>{item.emailId}</p>
         
        </div>
        {decode?.role==="admin"||decode?.role==="institute_admin"?<div  style={{display:'flex',justifyContent:'space-between',width:'3.5em'}}>
          <EditRole updateUser={item}/>
          <DeleteRole deleteUser={item}/>
         
        </div>:null}
      </div>
      )
     })}
     
    

    </div>
  )
}
