import React, { useState,useEffect } from 'react';

import axios from 'axios';
import {
  Alert,
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  Spinner,
  FormGroup,
} from 'reactstrap';
import dropdown from "../../../assets/images/dropdown.png"
import action3 from "../../../assets/images/action3.png"
import close from "../../../assets/images/close.png";
import { useSelector, useDispatch } from "react-redux"; import * as moment from 'moment'

import {userActions} from '../../../actions/user.actions';

const ApplicationStatusEnum = {
  InSubmission: 'In Submission',
  InReview: 'In Review',
  InSanction: 'In Sanction',
  InDisbursement: 'In Disbursement',
  Disbursed: 'Disbursed',
  Rejected: 'Rejected',
  Dropped: 'Dropped',
  OnHold: 'On Hold',
};
export default function MoveStatus(ApplicantId) {
  const statuschange = useSelector((state) => state.Updatestatus?.res)
  const droppedstate = useSelector((state) => state.Droppedstatuschange?.res)
  const disbursedstate = useSelector((state) => state.Disbursedstatuschange?.res)
  const reviewstate = useSelector((state) => state.Reviewstatuschange?.res)
  
  const dispatch = useDispatch();
  const user = sessionStorage.getItem("user")

  const id = ApplicantId.ApplicantId.applicationId


const name = ApplicantId.ApplicantId.name


  const [formLoader, setFormLoader] = useState(false);
  const [formStatusOpen, setStatusOpen] = useState(false);
  const [dropDownOpen, setDropDownOpen] = useState(false);
  const [descriptionText, setDescriptionText] = useState('');
  const [confirmationDialog, setConfirmationDialog] = useState(false);
  const [confirmationMessage, setConfirmationMessage] = useState('Hold On...');
  const [status, setStatus] = useState('Select Status');

  const updateDroppedStatusUrl = `/admin/application/update-status`;
  const updateInReviewStatusUrl = `/end-user/submit/admin`;

  
  const popUpStatusForm = () =>{
    setStatusOpen(true);

  }
  
  const handleStatusChange = () => {
    if (status === 'Status') {
      alert('NO STATUS CHANGE !');
    } else if (status === 'Dropped') {
     
      const droppedData = {
        userId: ApplicantId.ApplicantId.userId,
        applicationId: ApplicantId.ApplicantId.applicationId,
        status: ApplicationStatusEnum.Dropped,
        notes: descriptionText,
      };

      
      if(droppedstate){
        if (droppedstate.message === 'Successful') {
          setFormLoader(!formLoader);
          setStatusOpen(!formStatusOpen);
          setConfirmationDialog(!confirmationDialog);
          setConfirmationMessage('Dropped Successfully');
          setTimeout(() => setConfirmationDialog(!confirmationDialog), 2000);
          setTimeout(() => setConfirmationDialog(false), 2000);
          setTimeout(() => window.location.reload(true), 500);
        }else{
          setConfirmationMessage("error");
          setTimeout(() => setConfirmationDialog(!confirmationDialog), 2500);
           setTimeout(() => setConfirmationDialog(false), 2750);
        }
      }else{
        dispatch(userActions.fetchDroppedstatuschange(updateDroppedStatusUrl,droppedData,user),[])
      }
     
       
    } else if (status === 'In Review') {
            
        const reviewData = {
          userId: ApplicantId.ApplicantId.userId,
        };
        if(reviewstate){
          if (reviewstate.message === 'Successful') {
            setFormLoader(!formLoader);
            setStatusOpen(!formStatusOpen);
            setConfirmationDialog(!confirmationDialog);
            setConfirmationMessage('Moved To In Review Successfully');
            setTimeout(() => setConfirmationDialog(!confirmationDialog), 2000);
            setTimeout(() => setConfirmationDialog(false), 2000);
            setTimeout(() => window.location.reload(true), 500);
          }else{
            setConfirmationMessage("error");
            setTimeout(() => setConfirmationDialog(!confirmationDialog), 2500);
             setTimeout(() => setConfirmationDialog(false), 2750);
          }
        }else{
          dispatch(userActions.fetchReviewstatuschange(updateInReviewStatusUrl,reviewData,user),[])
        }
    } 
    else if (status === 'On Hold') {
            
     
      const statusData = {
        userId: ApplicantId.ApplicantId.userId,
        applicationId: ApplicantId.ApplicantId.applicationId,
        status: ApplicationStatusEnum.OnHold,
        notes: descriptionText,
      };
      
      
   
      if(droppedstate){
        console.log("inside first if")
        if (droppedstate.message === 'Successful') {
          setFormLoader(!formLoader);
            setStatusOpen(!formStatusOpen);
            setConfirmationDialog(!confirmationDialog);
          
          setConfirmationMessage('Moved to On Hold Successfully');
         setTimeout(() => setConfirmationDialog(!confirmationDialog), 2000);
          setTimeout(() => setConfirmationDialog(false), 2000);
          setTimeout(() => window.location.reload(true), 500);
        }else{
          setConfirmationMessage("error");
          setTimeout(() => setConfirmationDialog(!confirmationDialog), 2500);
           setTimeout(() => setConfirmationDialog(false), 2750);
        }
      }
       else{
        console.log("inside else")
        dispatch(userActions.fetchDroppedstatuschange(updateDroppedStatusUrl,statusData,user),[])
       }
        
      }
          
  };
  

  useEffect(()=>{
    handleStatusChange();
  },[droppedstate,reviewstate])
 
  

  const handleToggle = () => {
    setDropDownOpen(!dropDownOpen);
  };
  const handleDescriptionText = (text) => {
    setDescriptionText(text);
  };

  return (
    <div>
      <img  onClick={() => popUpStatusForm()} style={{cursor:'pointer',width:'1.1vw'}} src={action3} alt="resend consent"/>
      
      <Modal  isOpen={confirmationDialog}>
        <ModalHeader>Waiting For Confirmation</ModalHeader>
       
        <ModalBody>{confirmationMessage}</ModalBody>
      </Modal>
      {formStatusOpen && (
        <>
        <div
        style={{
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
          zIndex: 999,
        }}
      ></div>
      
      <Modal style={{
        
        width:"400px",
        
        top:"10%",
       
        
        borderRadius:"10px",
      }}
      isOpen={formStatusOpen}>
        <div style={{backgroundColor:"#E9E9E9",borderTopLeftRadius:'10px',borderTopRightRadius:'10px'}}>
            <div style={{display:"flex",flexDirection:"row",justifyContent:'space-between',height:'60px',padding:"10px"}}>
                <p style={{fontFamily:"Inter-Medium",fontSize:'14px'}}>Update Status Here ( Submission to <span style={{fontFamily:"Inter-Medium",color:'#D32028'}}>{status}</span> )</p>
                
                <img style={{height:'15px',width:'15px',marginTop:'12px'}} onClick={() => setStatusOpen(!formStatusOpen)} src={close}/>
            </div>
            </div>
           
            <div style={{marginTop:'10px'}}>
            <div style={{display:"flex",flexDirection:"row",justifyContent:"space-between",backgroundColor:'#FCF0F1',width:"100%",borderRadius:"10px"}}>
              <p style={{marginLeft:"15px",fontFamily:"Inter-Medium",fontSize:"14px",color:"#D32028"}}>Application ID <br/><span style={{fontFamily:"Inter-Medium",fontSize:"14px",color:"black"}}>{id}</span></p>
               <p style={{marginRight:"15px",fontFamily:"Inter-Medium",fontSize:"14px",color:"#D32028"}}> Applicant Name<br/><span style={{fontFamily:"Inter-Medium",fontSize:"14px",color:"black"}}>{name}</span></p>
            </div>
         
          
        </div>
        
        <ModalBody >
          <Form>
          <FormGroup>
              <p style={{lineHeight:'10px',fontFamily:"Inter-Medium",fontSize:'14px'}}>Select Status*</p>
              <Dropdown isOpen={dropDownOpen} toggle={() => handleToggle()}>
                <DropdownToggle style={{lineHeight:'10px',textAlign:"left",height:'40px',width:'330px',backgroundColor:'white',color:'gray',fontFamily:"Inter-Medium",fontSize:'14px'}} >
                  {status}<span style={{ position: 'absolute', right: '50px', top: '50%', transform: 'translateY(-50%)' }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12">
                      <path fill="currentColor" d="M1.5 4.5l4.5 4.5 4.5-4.5z" />
                    </svg>
                  </span>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem style={{fontFamily:"Inter-Medium",fontSize:'14px'}} onClick={(e) => setStatus(e.target.innerText)}>
                    Dropped
                  </DropdownItem>
                  <DropdownItem  style={{fontFamily:"Inter-Medium",fontSize:'14px'}}onClick={(e) => setStatus(e.target.innerText)}>
                    In Review
                  </DropdownItem>
                  <DropdownItem style={{fontFamily:"Inter-Medium",fontSize:'14px'}} onClick={(e) => setStatus(e.target.innerText)}>
                    On Hold
                  </DropdownItem>
              
                </DropdownMenu>
              </Dropdown>
              <p style={{marginTop:"20px",lineHeight:'1px',fontFamily:"Inter-Medium",fontSize:'14px'}}>Remarks</p>
              <input
                type="text"
                id="description"
                placeholder="Write Description Here"
                style={{borderRadius:"5px",borderStyle:"solid",borderWidth:"1px", height: '40px', width: '330px',fontFamily:"Inter-Medium",fontSize:'14px' }}
                onChange={(e) => handleDescriptionText(e.target.value)}
              />
            </FormGroup>

          </Form>
        </ModalBody>
        <ModalFooter style={{display:"flex",flexDirection:"row",justifyContent:"center",backgroundColor:"#E9E9E9",height:'60px'}}>

         
          <Button style={{backgroundColor:"#D32028",color:"white",fontFamily:"Inter-Medium",fontSize:"13px",border:"solid",borderRadius:"5px",borderWidth:"1px",height:"30px"}} disabled={formLoader} onClick={() => handleStatusChange()}>
            Make Changes
          </Button>
        </ModalFooter>
      </Modal>
      
      </>
      )}
    </div>
  );
}


