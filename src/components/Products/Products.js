import Sidebar from "../Sidebar"
import Topbar from "../Topbar"
import { useState, useEffect } from 'react';
import search from "../../assets/images/search.png"
import jwt_decode from 'jwt-decode';
import axios from 'axios';
import PaginationTable from "../status/PaginationTable";
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import axiosInstance from "../../helpers/axios";

import { useSelector, useDispatch } from "react-redux"; import * as moment from 'moment'

import ManageProducts from "./ManageProducts";
import {userActions} from '../../actions/user.actions';
import "./index.css"
import {
    Col,

    Input,

    Row,

  } from 'reactstrap';
  
import AddProducts from "./AddProducts"
const Products = () =>{
    const [filterInstituteName, setFilterInstituteName] = useState('');
    const user = sessionStorage.getItem('user');
    const decode=jwt_decode(user)
    const [tableData, setTableData] = useState([]);
    const [currentPage, setcurrentPage] = useState(1);
    const [rowsPerPage, setrowsPerPage] = useState(10);
    const [totalFetchRows, setTotalFetchRows] = useState(null);
    const ProcessingFeeTypeArray = ["", "Flat", "Percentage"];
    const FacilityTypeArray = ["", "Dicounted Interest", "Standard Interest", "Hybrid", "Business To Business"];
    const InterestTypeArray = ["", "Per Annum", "Flat"];
    const prodstate = useSelector((state) => state?.Products?.res);
    const dispatch = useDispatch();
    const getprod = `/products?perPage=${rowsPerPage}&pageNo=${currentPage}`;
    const [searchText, setSearchText] = useState("")
    const [isLoading, setIsLoading] = useState(true);
    useEffect(()=>{
    
    
      if(searchText === ""){
        
        const getprod = `/products?perPage=${rowsPerPage}&pageNo=${currentPage}`;

        axiosInstance.get(getprod, {
          headers: {
            Authorization: `Bearer ${user}`,
          },
        })
        .then((res) => {
         
          setTableData(res?.data?.data?.details);
          setcurrentPage(res?.data?.data?.pagination?.currentPage);
          setTotalFetchRows(res?.data?.data?.pagination?.totalRows);
          setrowsPerPage(res?.data?.data?.pagination?.perPage);  
          const loadingTimeout = setTimeout(() => {
            setIsLoading(false);
          }, 300)
        })
        .catch((error) => {
         
          console.error("Error fetching filtered data:", error);
        });
        }
        
    
    },[])
    const [id,setId]=useState(null)
  const getInsituteInfo=(text)=>{
    
    const urlGetInstitute=`/institute/info/name?name=${text}`;
      
    if(text.length>=4)
    {
      axiosInstance.get(urlGetInstitute,{
        headers:{
          Authorization:`Bearer ${user}`
        }
      }).then((res) => {
        console.log("text",res?.data?.data[0]?.id)
        setId(res?.data?.data[0]?.id)
      });
  
   
    }
 
  }
  const handleInputChange = (event) => {

    if(event.target.value.length >= 4){
      setSearchText(event.target.value)
    
      const getprod = `/products?instituteId=${id}&perPage=${rowsPerPage}&pageNo=${1}`;

  console.log("url",getprod)
    axiosInstance.get(getprod, {
      headers: {
        Authorization: `Bearer ${user}`,
      },
    })
    .then((res) => {
      console.log("filtering",res)
      setTableData(res?.data?.data?.details);
      setcurrentPage(res?.data?.data?.pagination.currentPage);
      setTotalFetchRows(res?.data?.data?.pagination.totalRows);
      setrowsPerPage(res?.data?.data?.pagination.perPage);  
   
    })
    .catch((error) => {
      console.error("Error fetching filtered data:", error);
    });
  }
  else{
    setSearchText("")
    const getprod = `/products?perPage=${rowsPerPage}&pageNo=${currentPage}`;

    axiosInstance.get(getprod, {
      headers: {
        Authorization: `Bearer ${user}`,
      },
    })
    .then((res) => {
      console.log("filtering",res)
      setTableData(res?.data?.data?.details);
      setcurrentPage(res?.data?.data?.pagination.currentPage);
      setTotalFetchRows(res?.data?.data?.pagination.totalRows);
      setrowsPerPage(res?.data?.data?.pagination.perPage);  
   
    })
    .catch((error) => {
      console.error("Error fetching filtered data:", error);
    });



  }
};
const paginate = (pageNumber) => {


  console.log("Search Text", searchText)
  // setcurrentPage(pageNumber);
  if(searchText.length >= 4){

    const getprod = `/products?instituteId=${id}&perPage=${rowsPerPage}&pageNo=${pageNumber}`;
    console.log("url",getprod)
  axiosInstance.get(getprod, {
    headers: {
      Authorization: `Bearer ${user}`,
    },
  })
  .then((res) => {
    console.log("filtering",res)
    setTableData(res?.data?.data?.details);
    setcurrentPage(res?.data?.data?.pagination?.currentPage);
    setTotalFetchRows(res?.data?.data?.pagination?.totalRows);
    setrowsPerPage(res?.data?.data?.pagination?.perPage);  
 
  })
  .catch((error) => {
    console.error("Error fetching filtered data:", error);
  });

  }
  else{
    const getprod = `/products?perPage=${rowsPerPage}&pageNo=${pageNumber}`;
  


  axiosInstance.get(getprod, {
    headers: {
      Authorization: `Bearer ${user}`,
    },
  })
  .then((res) => {
  
    setTableData(res?.data?.data?.details);
    setcurrentPage(res?.data?.data?.pagination?.currentPage);
    setTotalFetchRows(res?.data?.data?.pagination?.totalRows);
    setrowsPerPage(res?.data?.data?.pagination?.perPage);  
 
  })
  .catch((error) => {
    console.error("Error fetching filtered data:", error);
  });
  }
  
};
const [isOpen,setIsOpen]=useState(true)

    return(
          <div style={{display:'flex', flexDirection:'row',width:"100%",backgroundColor:'white',minHeight:'100vh'}}>
        <Sidebar isOpenSidebar={isOpen} handler={setIsOpen}/>
        <div style={{width:isOpen?'77%':'97%', overflow:"auto",marginLeft:isOpen?"22%":'0%',}}>
            <Topbar/>
            <div>
            <h1 style={{fontFamily:'Inter-Medium',fontSize:'1vw',fontWeight:'500',color:'#D32028',marginTop:'10px'}}>Products</h1>

            <Row>
       <div style={{ display: 'flex', flex: 'wrap',marginTop:'10px',flexDirection:'row',justifyContent:'space-between' }}>
         <div>
         <Col sm="12" md="10" lg="3">
         <div style={{ height: '23px', marginRight: '10px',fontFamily:'Inter'  , position: 'relative', display: 'flex', alignItems: 'center',width:'250px' }}>
         <Input
           type="text"
           placeholder="Institute Name"
           onChange={(event) => {
             getInsituteInfo(event.target.value);
             handleInputChange(event);
           }}
           style={{
              fontSize: '0.8vw',
             paddingLeft: '26px',
             height: '23px',
             fontFamily: 'Inter',
             backgroundImage: `url(${search})`,
             backgroundRepeat: 'no-repeat',
             backgroundPosition: '5px center',
             backgroundSize: '15px 15px',
             paddingRight: '5px', 
             borderColor:'#cccccc',
             borderStyle:'solid',
             borderRadius:"5px",
             borderWidth:'1px',
             height:'34px',
             marginTop:'11px',marginLeft:'3px'
           }}
         />
       </div>
         </Col>
         </div>
         <div>
         <Col sm="12" md="10" lg="3" style={{marginLeft:'60%'}}>
           <AddProducts/>
         </Col>
         </div>
         <div>
         <Col sm="12" md="10" lg="3" >
           <ManageProducts/>
         </Col>
         </div>
         
       </div>
     </Row>
     {isLoading ? (
       <Box style={{ display: 'flex',alignItems:'center',justifyContent:'center',padding:'15%',backgroundColor:'white',height:'100vh'}}>
       <CircularProgress />
     </Box>
     ):(
<>
<div className='tables'  style={{marginTop:'20px'}}>
     <table >
           <thead className='table-heading' style={{width:'100%'}} >
             <tr >
              
               <th className="thead-text" style={{  fontSize: '0.9vw',fontFamily:"Inter" ,width:'12em',borderTopLeftRadius:'8px'}} id="insName">
             <span style={{marginLeft:'10px'}}> Product</span> 
               </th>

               <th className="thead-text" style={{  fontSize: '0.9vw' ,fontFamily:"Inter",width:'18em',paddingLeft:'10px'}} id="coursename">
               Institute
               </th>

               <th className="thead-text" style={{  fontSize: '0.9vw' ,fontFamily:"Inter",width:'8em',paddingLeft:'10px'}} id="coursename">
               Tenure
               </th>
               <th className="thead-text" style={{  fontSize: '0.9vw' ,fontFamily:"Inter",width:'9em'}} id="interest">
               Subvention
               </th>
               <th className="thead-text" style={{  fontSize: '0.9vw' ,fontFamily:"Inter",width:'9em'}} id="interest">
               Standard Int.
               </th>
               <th className="thead-text" style={{  fontSize: '0.9vw' ,fontFamily:"Inter",width:'9em'}} id="interest">
               Student Share
               </th>
               <th className="thead-text" style={{  fontSize: '0.9vw' ,fontFamily:"Inter",width:'6em',paddingLeft:'10px'}} id="pf">
               PF
               </th>
               <th className="thead-text"  style={{  fontSize: '0.9vw',width:'9em',cursor:'pointer',fontFamily:"Inter" }} id="advEmi">
                 Advance EMIs
               </th>
               <th className="thead-text"  style={{  fontSize: '0.9vw',width:'8em',cursor:'pointer',fontFamily:"Inter" }} id="appfromdate">
                   Interest Type
               </th>
               <th className="thead-text"  style={{  fontSize: '0.9vw',width:'8em',cursor:'pointer',fontFamily:"Inter",borderTopRightRadius:'8px' }} id="status">
                   <span style={{marginLeft:'10px'}}> Status</span> 
               </th>
               

              
               {/* <th className="thead-text"  style={{  fontSize: '13.5px',width:'20em',cursor:'pointer',fontFamily:"Inter" }} id="appfromdate">
                 Action
               </th> */}
             </tr>
           </thead>

       
           
           {tableData?.map((item,index)=>{
               const filterInstitute = filterInstituteName.toLowerCase();
               const currentInstituteName = (item.instituteName || '').toLowerCase();
               if (currentInstituteName.indexOf(filterInstitute) !== -1)
             return(
                 <tbody className='table-body'>
                 <tr  className='table-row' key={index} style={{ lineHeight: '14px' }}>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw',paddingLeft:'10px'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#101828',fontWeight:'500'}}>{item?.name}</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw',paddingLeft:'10px'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{item?.instituteName}</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw',paddingLeft:'10px'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{item?.tenure}</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{(item?.discountedInterestPercentage*100).toFixed(2)}%</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{(item?.standardInterestPercentage*100).toFixed(2)}%</span></td>
                 <td style={{ color: '#667085', fontFamily: 'Inter', fontSize: '0.9vw' }}>
                  <span style={{ fontFamily: 'Inter', fontSize: '0.9vw', color: '#667085', fontWeight: '500' }}>
                    {item?.facilityType === 3 ? item.studentShare : 0}
                  </span>
                </td>                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{item?.processingfeeValue}</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{item?.advanceEmis}</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{item?.facilityType===1||item?.facilityType===5?InterestTypeArray[item?.discountedInterestType]:item?.facilityType===2?InterestTypeArray[item?.standardInterestType]:item?.facilityType===3?`${InterestTypeArray[item?.discountedInterestType]}`:null}</span></td>
                 <td style={{color:'#667085',fontFamily:'Inter',fontSize:'0.9vw'}}><span style={{fontFamily:'Inter',fontSize:'0.9vw',color:'#667085',fontWeight:'500'}}>{item?.status===1?"Active":"In-Active"}</span></td>
                

                 {/* <td title="Manage Institute"><span style={{cursor:'pointer'}}><ManageProducts/></span></td> */}
                 </tr>
                
                 </tbody>
             )
          
                 })}
           
        
         </table>
     </div>
    
     {totalFetchRows ? ( searchText && totalFetchRows <= 5 ?(
               <div style={{ textAlign: 'center', fontWeight: 'bold',fontFamily:"Inter", fontSize: '0.9vw',marginLeft:'10px',marginBottom:'5px' }}>
               Showing {totalFetchRows}-{totalFetchRows}{' '}
               records
             </div>
            ):( <div style={{ textAlign: 'center', fontWeight: 'bold',fontFamily:"Inter", fontSize: '0.9vw',marginLeft:'10px',marginBottom:'5px' }}>
            Showing {currentPage * rowsPerPage + 1 - rowsPerPage}-{currentPage * rowsPerPage}{' '}
            records
          </div>)
             
            ) : (
              <div style={{ textAlign: 'center', fontWeight: 'bold', fontFamily:"Inter", fontSize: '0.9vw' }}>No Records</div>
            )}
         <PaginationTable
           startPage={searchText !== "" ? 1 : currentPage}
           rowsPerPage={rowsPerPage}
           totalRows={totalFetchRows}
           paginate={paginate}
           searchText={searchText}
         />
</>
     )}
     
            </div>
          </div>
     </div>
        
 
       
    )
}
export default Products