/* eslint-disable no-restricted-globals */
/* eslint-disable no-bitwise */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable react/void-dom-elements-no-children */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable eqeqeq */
/* eslint-disable import/extensions */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {userActions} from '../../actions/user.actions'
import axios from "axios";
import { useLocation } from 'react-router-dom';
import plus from "../../assets/images/plus_icon.png"
import close from "../../assets/images/close.png"
import edit from "../../assets/images/edit.png"
import {
  
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  FormText,
  Form,
  Input ,
  FormGroup,
  Label,
  Row,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Col,
} from 'reactstrap';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import classnames from 'classnames';
import './index.css'




export default function ManageProducts(){

  const dispatch=useDispatch();

  const instituteState=useSelector((state)=>state?.GETINSTITUTE?.res?.data?.data)
  const productsListState=useSelector((state)=>state?.GETPRODUCTS?.res?.data?.data)
  const [formOpen, setFormOpen] = useState(false);


  useEffect(() => {

    if(productsListState)
    {

      setProductList(productsListState)
    }
  }, [productsListState])
  
const getInsituteInfo=(text)=>{
  
  const urlGetInstitute=`/institute/info/name?name=${text}`;
    
  if(text.length>=4)
  {
   dispatch(userActions.getInstituteInfo(urlGetInstitute,user))
   setInstituteList(instituteState)
  }
  else{
    setInstituteList(null)
    setStarterPhrase("Type Institute Name")
  }
}

  const [load,setLoading]=useState(false)
   const [status, setStatus] = useState("Select Category of Product");
const user =sessionStorage.getItem('user')
  

  const FacilityTypeArray = ["", "Dicounted Interest", "Standard Interest", "Hybrid", "Business To Business"];
  const InterestTypeArray = ["", "Per Annum", "Flat"];
  const ProcessingFeeTypeArray = ["", "Flat", "Percentage"];

  const [activeTab, setActiveTab] = useState('1');
  const location = useLocation();
  const [open, setOpen] = useState(null);
  const toggle = (id) => {
    if (open === id) {
      setOpen();
    } else {
      setOpen(id);
    }
  };


  

  const validateEmail = (email) => {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  
const [productCode,setProductCode]=useState(null)
  



//get institute Id
const [instituteList,setInstituteList]=useState(null)
const [startingPhrase,setStarterPhrase]=useState("Type Institute Name")
const [instituteCode,setInstituteCode]=useState(null)

 

//status

const [productStatus,setProductStatus]=useState(null)
const [currentInstituteId,setCurrentInstituteId]=useState(null)

const handleProductStatus=(e)=>{
 
  
  setProductStatus(e)
}
const handleProductStatusChange=(productId)=>{
const updateProductStatus=`${process.env.REACT_APP_BASE_URL}/products/status?productId=${productId}&status=${productStatus}`

 const urlProductList=`/products/instituteId?instituteId=${currentInstituteId}`
axios.post(updateProductStatus,null,{
  headers:{
    Authorization:`Bearer ${user}`
  }
})
.then((res)=>{
  alert("Sucess")
  dispatch(userActions.getProductList(urlProductList,user))
})
.catch((err)=>{
  alert('Error', err)

})

}
//search products

const [productsList,setProductList]=useState(null)
const [searchInstituteName,setSearchInstituteName]=useState(null)

const getProductDetails=(text)=>{
  const instituteInfoUrl=`${process.env.REACT_APP_BASE_URL}/institute/info/name?name=${text}`;
  if(text.length>=4)
  {
    axios.get(instituteInfoUrl,{
      headers:{
        Authorization:`Bearer ${user}`
      }
    }).then((res)=>{
      if(res.data.data==[])
      {
        setInstituteList(null)
        setStarterPhrase("No institute Found")
      }
      else
      setInstituteList(res.data.data)

    })
  }
  else{
    setInstituteList(null)
    setStarterPhrase("Type Institute Name")
  }
}

//Creating Product API
const [instituteID,setInstituteID]=useState(null)
const [productName,setProductName]=useState(null)
const [studShare,setStudentShare]=useState(null)
const [instShare,setInstituteShare]=useState(null)
const [FLDGpercentage,setFldgPercentage]=useState(null)
const [pfTypeValue,setPfTypeValue]=useState(null)
const [IRR,setIRR]=useState(null)
const [dir,setDIR]=useState(null)
const [sir,setSIR]=useState(null)
const [ir,setIR]=useState(null)

const handleInstituteDetails=(data)=>{
  document.getElementById("instituteNameBox").value=data.name
  setInstituteCode(data.code)
  setInstituteID(data.id)
  setInstituteList(null)

}

const handleSearchInstituteDetails=(data)=>{
  
  setCurrentInstituteId(data.id)

  const urlProductList=`/products/instituteId?instituteId=${data.id}`

  dispatch(userActions.getProductList(urlProductList,user))

  
  setInstituteList(null);
  
  document.getElementById("searchInstituteNameBox").value=data.brandName
  

}

const popUpForm = () => {
  setFormOpen(true);
}

  return (
    <div>
<p style={{fontFamily:'Inter-Medium',fontSize:'16px',fontWeight:'500', textTransform: 'capitalize',cursor:'pointer',width:'180px',height:'34px',fontFamily:'Inter-Medium',fontSize:'16px',backgroundColor:'#D22129',display:'flex',alignItems:'center',justifyContent:'center',color:'white',borderRadius:'6px'}} onClick={() => popUpForm()}><img  src={edit} style={{marginRight:'10px',height:'15px',width:'15px'}}/>Manage Product</p>
{formOpen && (
  <>
   <div
        style={{
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
          zIndex: 999,
        }}
      ></div>
      <Modal style={{
        position: 'absolute',
        top: '0%',
      left: '10%',
      right :'10%',
      bottom : '10%',
        
        backgroundColor: 'white',maxWidth: '100%'}} size="lg" className='edit-form ' isOpen={formOpen}>
      <div >
 
 <TabContent className="p-4" activeTab={activeTab}>
   <TabPane tabId="1">

     <Row>
      
           
       <div  style={{width:"100%",display:'flex'}}>
       
         <p style={{width:'10em',fontWeight:'bold'}}>Institute Name</p>
          <div style={{display:"block" }}>
         <div><Input id="searchInstituteNameBox" onChange={(e)=>getInsituteInfo(e.target.value)} type="text" placeholder="Search Institute Products" style={{border:'1px solid #F0F0F0',boxShadow:'0px 0px 1px 1px #D0D0D0',width:'25em'}}/>
         </div>
         <div style={{marginTop:'1em'}}>
         {
             instituteList?instituteList.map((item)=>{
             return(
               <div onClick={()=>handleSearchInstituteDetails(item)} className="instituteName" style={{cursor:'pointer',border:'1px solid grey',borderRadius:'0.5em'}}>
               <p  style={{fontWeight:'600',paddingTop:'0.5em',paddingLeft:'0.5em'}}>{item.brandName}</p>
               </div>
             )
             }
             ):null

         }
         </div>
       </div>
           {/* <div style={{display:'flex' ,marginLeft:'30%',height:'2.5em'}}>
               <p style={{width:'8em',marginTop:'0.3em',marginLeft:"2em",fontWeight:'500'}}>Product ID</p><Input disabled type="number" placeholder="Search Product by ID" style={{border:'1px solid grey',cursor:'not-allowed',width:'20em'}}/> 
             <Button  style={{marginLeft:'1em',backgroundColor:'#d32028',border:'none'}}onClick={()=>setConfirmationDialog(!confirmationDialog)}> Add Product </Button>
           </div> */}
         <img style={{height:'1.5em',width:'1.5em',marginLeft:'50%'}}  className="image1" onClick={()=>setFormOpen(!formOpen)} src={close}/>

     </div>
     
     </Row>
     <br/>

   </TabPane>
 </TabContent>


     <Row>
       <div>

{ productsList?productsList.map((item,idx)=>{

  

  return( 
  <>
  <div style={{boxShadow:'0px 0px 5px 0px rgb(0,0,0.3,0.6)',padding:'7px',marginBottom:'8px',marginLeft:'5px',marginRight:'5px'}}>
     <div style={{display:'flex',paddingLeft:'2em'}}>
  <select  onChange={(e)=>handleProductStatus(e.target.value)}>
    <option name="Change Status" value="Change Status">Change Status</option>
    <option name="In-Active" value={0}>In-Active</option>
    <option name="Active" value={1}>Active</option>
  </select>
            <Button style={{height:'2.5em',marginLeft:'1em'}} onClick={()=>handleProductStatusChange(item.productId)}>Submit</Button>
</div>
  <Accordion style={{marginBottom:'1em',marginTop:'10px',boxShadow:'0px 0px 2px 2px #F0F0F0',margin:'2em'}} key={item.productId}>
 <AccordionSummary
   expandIcon={<ExpandMoreIcon />}
   aria-controls="panel2a-content"
   id="panel2a-header"
 >
   <Typography><span style={{display:'flex',justifyContent: 'space-between',}}><h5 style={{color:"#d32028",width:'14em'}}>Product ID: {item.name} </h5> <h5 style={{width:'14em'}}>Tenure: {item.tenure}</h5><h5 style={{width:'14em'}}>Status: {item.status?"ACTIVE":"IN-ACTIVE"}</h5></span></Typography>
 </AccordionSummary>
 <AccordionDetails>
   <Typography>
    <div style={{display:'flex',justifyContent:'space-around'}}>
   
     <div  style={{display:'block'}}>
     <p><strong>Advance EMI :</strong> {item.advanceEmis}</p>
     <p><strong>Interest Type :</strong> {item.facilityType===1||item.facilityType===5?InterestTypeArray[item.discountedInterestType]:item.facilityType===2?InterestTypeArray[item.standardInterestType]:item.facilityType===3?`${InterestTypeArray[item.discountedInterestType]}`:null}</p>
     <p><strong>Facility Type :</strong> {FacilityTypeArray[item.facilityType]}</p>
     <p><strong>Tenure :</strong> {item.tenure}</p>
     </div>
     <div style={{display:'block'}}>
     <p><strong>Rate :</strong> {item.facilityType===1||item.facilityType===5?item.discountedInterestPercentage:item.facilityType===2?item.standardInterestPercentage:item.facilityType===3?`${item.discountedInterestPercentage} student share : ${item.studentShare}`:null}</p>
     <p><strong>GST (yes/no) :</strong> {item.processingfeeValueGST===0?"No":`Yes ${item.processingfeeValueGST}`}</p>
     <p><strong>Processing Fee :</strong> {item.processingfeValueIncludingGST}  ({ProcessingFeeTypeArray[ item.processingfeeType]}) </p>
     <p><strong>Advance EMI :</strong> {item.advanceEmis}</p>
     </div>
 </div>
     <div style={{display:'block',backgroundColor:'#FFF8F4',boxShadow:'0px 0px 2px 2px  #F9D8D6',borderRadius:'10px',padding:'1em',width:'50%',marginLeft:'20%'}}>
       <div style={{display:'block'}}>
         <ul style={{display:'flex',justifyContent:'space-between',textDecoration:'none',listStyle:'none'}}> 
           <li style={{width:'10em',color:'#d32028',fontWeight:'bold'}}>Disburse Month</li>
           <li style={{width:'5em'}}>{item.disbursementMonth1}</li>
           <li style={{width:'5em'}}> {item.disbursementMonth2}</li>
           <li style={{width:'5em'}}> {item.disbursementMonth3}</li>
           <li style={{width:'5em'}}> {item.disbursementMonth4}</li>
       </ul>
        <ul style={{display:'flex',justifyContent:'space-between',listStyle:'none'}}> 
           <li style={{width:'10em',color:'#d32028',fontWeight:'bold'}}>Percentage</li>

             <li style={{width:'5em'}}> {item.disbursementMonth1Percentage}</li>
               <li style={{width:'5em'}}> {item.disbursementMonth2Percentage}</li>
               <li style={{width:'5em'}}> {item.disbursementMonth3Percentage}</li>
               <li style={{width:'5em'}}> {item.disbursementMonth4Percentage}</li>
       </ul>
    
         
       </div>

      
     </div>
   </Typography>
 </AccordionDetails>
</Accordion>
</div>
</>



)}):null
               
}

</div>
     </Row>

</div>
      </Modal>
  </>
)}

    </div>
    
  );
};


