import React, {useEffect,useState} from 'react'
import axiosInstance from '../../../helpers/axios';

import {
    Col,
    Row,
} from 'reactstrap';

const PreScreen = ({userId,applicationId})=>{
    const user=sessionStorage.getItem('user')
  const [cibilDetails,setCibilDetails]=useState(null)

    function formatNumberWithCommas(number) {
        return number?.toLocaleString('en-IN'); 
      }
  const preScreenUrl = `/experian/report?userId=${userId}&applicationId=${applicationId}`

  useEffect(() => {
    axiosInstance.get(preScreenUrl, {
        headers: {
          Authorization: `Bearer ${user}`,
        },
      }).then((res=>{ setCibilDetails(res?.data?.data)}
     )).catch((error)=>(console.log("error",error)))
  }, [userId,applicationId])
  
 
     
    
    return(
        <>
                    <Row style={{marginTop:'20px'}}>
                        <div style={{display:'flex',justifyContent:'space-between',paddingLeft:'12px',paddingRight:'10px',paddingTop:"10px"}}>
                            <div>
                                <Col sm={4}>
                            <div style={{borderStyle:'solid',borderWidth:'1px',borderColor:'white',borderRadius:'8px',padding:'20px',boxShadow: '0px 0px 2px 2px  #f0f0f0',width:'250px'}}>
                                <p style={{fontFamily:'Inter-Medium',color:'#667085'}}>summary_bureau_name</p>
                                <p style={{fontFamily:'Inter-Medium',fontSize:'1vw',lineHeight:'1em'}}>{cibilDetails?.summary?.bureau_name}</p>
                            </div>
                                </Col>
                            </div>
                            <div>
                                <Col sm={4}>
                                <div style={{borderStyle:'solid',borderWidth:'1px',borderColor:'white',borderRadius:'8px',padding:'20px',marginLeft:'7px',boxShadow: '0px 0px 2px 2px  #f0f0f0',width:'250px'}}>
                                <p style={{fontFamily:'Inter-Medium',color:'#667085'}}>Score Range</p>
                                <p style={{fontFamily:'Inter-Medium',fontSize:'1vw',lineHeight:'1em'}}>{cibilDetails?.summary?.bureau_score_bucket}</p>
                            </div> 
                                </Col>
                            </div>
                            <div>
                                <Col sm={4}> 
                                <div style={{borderStyle:'solid',borderWidth:'1px',borderColor:'white',borderRadius:'8px',padding:'20px',marginLeft:'7px',boxShadow: '0px 0px 2px 2px  #f0f0f0',width:'250px'}}>
                                <p style={{fontFamily:'Inter-Medium',color:'#667085'}}>obligation</p>
                                <p style={{fontFamily:'Inter-Medium',fontSize:'1vw',lineHeight:'1em'}}>{cibilDetails?.obligation}</p>
                            </div>
                                
                                </Col>
                            </div>
                            <div>
                                <Col sm={4}> 
                                <div style={{borderStyle:'solid',borderWidth:'1px',borderColor:cibilDetails?.summary?.max_dpd_ever>30?"#d32028":"#fff",borderRadius:'8px',padding:'20px',marginLeft:'7px',boxShadow: '0px 0px 2px 2px  #f0f0f0',width:'250px',backgroundColor:cibilDetails?.summary?.max_dpd_ever>30?"#Feedee":"#fff"}}>
                                <p style={{fontFamily:'Inter-Medium',color:'#667085'}}>Max DPD days</p>
                                <p style={{fontFamily:'Inter-Medium',fontSize:'1vw',lineHeight:'1em'}}>{cibilDetails?.summary?.max_dpd_ever}</p>
                            </div>
                                
                                </Col>
                            </div>
                            
                        </div>
                    </Row>
                    <Row style={{marginTop:'20px'}}>
                    <Col sm={2} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: parseInt(cibilDetails?.dpd_risk_flag)?"#d32028":"#fff",backgroundColor:parseInt(cibilDetails?.dpd_risk_flag)?"#Feedee":"#fff", borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' }}>dpd_risk_flag</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', lineHeight: '1em',color:cibilDetails?.dpd_risk_flag?"#d32028":"#000" }}>{cibilDetails?.dpd_risk_flag}</p>
                        </div>
                    </Col>
                    <Col sm={2} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' }}>summary_wilful_default_flag</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', lineHeight: '1em',color:cibilDetails?.summary?.is_suitfiled_willfuldefault_writtenoffstatus_last24months?"#d32028":"#fff"}}>{cibilDetails?.summary?.is_suitfiled_willfuldefault_writtenoffstatus_last24months.toString()}</p>
                        </div>
                    </Col>
                    <Col sm={2} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%',backgroundColor: cibilDetails?.summary?.is_suitfiled_wilfuldefault_last24months?"#FEEDEE":"#fff", }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' }}>summary_suit_filed_flag</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', lineHeight: '1em',color: cibilDetails?.summary?.is_suitfiled_wilfuldefault_last24months?"#d32028":"#000"}}>{cibilDetails?.summary?.is_suitfiled_wilfuldefault_last24months.toString()}</p>
                        </div>
                    </Col>
                    <Col sm={2} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' }}>summary_settled_written_off_flag</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', lineHeight: '1em' }}>{cibilDetails?.summary?.settled_written_off_flag}</p>
                        </div>
                    </Col>
                    <Col sm={2} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' }}>summary_written_off_gt_1000_flag</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', lineHeight: '1em' }}>{cibilDetails?.summary?.written_off_gt_1000_flag}</p>
                        </div>
                    </Col>
                    <Col sm={2} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' }}>summary_written_off_flag</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', lineHeight: '1em' }}>{cibilDetails?.summary?.written_off_flag}</p>
                        </div>
                    </Col>
                    </Row>
                    <Row style={{marginTop:'20px'}}>
                    <Col sm={4} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' , lineHeight: '1em'}}>Active / All Loans / Overdue Accounts</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1em' }}>{cibilDetails?.summary?.cnt_active_accounts} / {cibilDetails?.summary?.cnt_all_accounts} / {cibilDetails?.summary?.cnt_overdue_accounts}</p>
                        </div>
                    </Col>
                    <Col sm={4} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085' , lineHeight: '1em' }}>Oustanding / Sanctioned</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw' }}>{formatNumberWithCommas(cibilDetails?.summary?.total_outstanding_amount)} / {formatNumberWithCommas(cibilDetails?.summary?.sum_tradelines_balance)}</p>                        </div>
                    </Col>
                    <Col sm={4} style={{ marginBottom: '15px' }}>
                        <div style={{ borderStyle: 'solid', borderWidth: '1px', borderColor: 'white', borderRadius: '8px', padding: '20px', boxShadow: '0px 0px 2px 2px  #f0f0f0', height: '100%' }}>
                        <p style={{ fontFamily: 'Inter-Medium', color: '#667085', lineHeight: '1em'  }}>Write Offs</p>
                        <p style={{ fontFamily: 'Inter-Medium', fontSize: '1vw',color:'#d32028' }}>{cibilDetails?.summary?.cnt_writtenoff} / {cibilDetails?.summary?.total_writtenoff_amt}</p>
                        </div>
                    </Col>
                   
                    </Row>
                    <div className='tables' style={{ marginTop: '20px' }}>
                        <table hover  style={{width:'100%'}}>
                            <thead style={{backgroundColor:'#F9FAFB'}}>
                                <tr >
                                    <th style={{fontFamily:'Inter-Medium',fontSize:'1vw',borderTopLeftRadius:'8px',paddingLeft:'10px',width:'25%'}}>Name</th>
                                    <th style={{fontFamily:'Inter-Medium',fontSize:'1vw',width:'65%'}}>Description</th>
                                    <th style={{fontFamily:'Inter-Medium',fontSize:'1vw',borderTopRightRadius:'8px',width:'10%',paddingRight:'5px',whiteSpace:'nowrap'}}>Response Value</th>
                                </tr>
                            </thead>
                            <tbody  >
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Sanctioned L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max Sanctioned Balance of All Loans Reported in the last 2 years</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{formatNumberWithCommas(cibilDetails?.summary?.tradeline_max_balance_24months)}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Settled/Write offs Total L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Total Settlement Written Off in Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.settlement_writtenoff_last24months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Overdue Accounts</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Total Overdue Accounts in Bureau</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_overdue_accounts}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Suite Filed/Wilfil L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Suit Filed/Wilful Default Loans Reported in the Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_overdue_accounts}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Settled/Write offs Count L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count Suit Filed/Written off Accounts Reported in the Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_settlement_writtenoff_default_last24months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Inquiries L1M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Credit Enquiries of Business Loans in Bureau Report in Last 1 month</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_bl_inquiries_last_1m}</td>

                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Overdue Accounts</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Accounts Reported Overdue Having Overdue Amount greater than 100</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_overdue_loans}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>DLSW LTM</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Accounts with DPD string having 'DBT','LSS','SUB','WOF' reported in last 12 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_doubtfl_loss_substd_spcl_mention_acc_last12month}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Highest Overdude</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max Overdue Amount of Accounts Reported Overdue</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_overdue}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Suite Filed/Wrttren off</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Suit Filed and Written Off Accounts Reported</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.settlement_writtenoff}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Settled Written off L12M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Suit Filed and Written Off Accounts Reported in the Last 12 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.settlement_writtenoff_last12months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Settled Written off L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Suit Filed and Written Off/Wilful Default Reported in the Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.settlement_writtenoff_last24months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Suitefiled Wilful L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Flag of Suit Filed/Wilful Default Loans Reported in the Last 24 months</td>
                                    <td style={{ fontFamily: 'Inter-Medium', fontSize: '1vw', color: '#667085' }}>
                                    {cibilDetails?.summary?.is_suitfiled_wilfuldefault_last24months.toString()}
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>SFWFDWO L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Suit Filed and Written Off/Wilful Default Accounts Reported in the Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.is_suitfiled_willfuldefault_writtenoffstatus_last24months.toString()}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Written off L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Total Written Off Amount of Accounts Reported in the Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.amts_writtenoff_last24months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Active Overdue</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Total Overdue Amount of Active Loans Reported as Overdue</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.sum_active_overdue_amount}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Overdue - Non CC_CL_GL</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Total Overdue Amount of Active Loans Reported as Overdue for All Accounts other than Credit Card Accounts, Consumer Loans and Gold Loans</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.sum_active_overdue_amount_non_cc_cd_gl}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Inquiries L7D</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Credit Enquries in Bureau Report in Last 7 days</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.enquiry_last7days}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Max DPD in Current Active Accounts</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Current Active Max DPD of Customer</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_dpd_current_active_account}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Max DPD L6M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max DPD Reported in Last 6 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_dpd_last_6_mo}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Max DPD L12M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max DPD Reported in Last 12 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_dpd_inlast12months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Max DPD L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max DPD Reported in Last 24 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_dpd_inlast24months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Max DPD L36M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max DPD Reported in Last 36 months</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_dpd_inlast36months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Max DPD All Time</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Max DPD Reported Ever</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.max_dpd_ever}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>Defaulted EMI %</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Percentage of Instalments reported to be defaulted in Bureau</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.percentage_installments_defaulted?.toFixed(2)}%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>DPD90 Accounts L24M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Accounts Reported in the Last 24 months having Days Past Due greater than or equal to 90</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_90plus_dpd_last24months}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td colSpan="12" style={{ borderTop: '1px solid #ccc' }}></td>
                                </tr>
                                <tr className="table-row" style={{ lineHeight: '40px' }}>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',paddingLeft:'10px',color:'#667085'}}>DPD90 Accounts L36M</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>Count of Accounts Reported in the Last 36 months having Days Past Due greater than or equal to 90</td>
                                    <td style={{fontFamily:'Inter-Medium',fontSize:'1vw',color:'#667085'}}>{cibilDetails?.summary?.cnt_90plus_dpd_last36months}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
        </>
    )
}
export default PreScreen