 
import React,{useState} from 'react';
import axiosInstance from '../../../helpers/axios'
import jwt_decode from 'jwt-decode'
import {
  
  Modal,
  ModalBody,
  Button,
  ModalHeader,
} from 'reactstrap';
import { TransformWrapper,TransformComponent} from 'react-zoom-pan-pinch';

export default function ViewFile({currUserId,currApplicationId,type,item,image , item2}) {

  const user=sessionStorage.getItem('user')
  const decode=jwt_decode(user)
  const [imageOpen, setImageOpen] = useState(false);
 



  return (
    <>
    <Modal style={{overflow:'auto',height:'40em'}} size="xl" isOpen={imageOpen}>
        <ModalHeader >Uploaded File <i>( Use Scroll to Zoom in the Picture )</i> <Button style={{position: 'absolute',right:'2em'}} type="button" onClick={()=>setImageOpen(!imageOpen)}>X</Button></ModalHeader>
        <ModalBody  style={{margin:'5%',border:'2px dashed #D0D0D0',display:'block',justifyContent:'center'}} >

        {item?.substring(item?.lastIndexOf(".")+1)==="png"||item?.substring(item?.lastIndexOf(".")+1)==="jpeg"||item?.substring(item?.lastIndexOf(".")+1)==="jpg"?
         <TransformWrapper
            defaultScale={1}
           >
            <TransformComponent>
               <img style={{width:'100%'}} src={item} alt="File"/>
               {item2?<img src={item2} style={{width:'100%'}} alt="file"/>:null}
            </TransformComponent>
        </TransformWrapper>:
        <a target="_blank" rel="noreferrer" href={item?item:item2}>Click To Download</a>

          } 
     
        
      
          </ModalBody>
      </Modal>
      <img src={image} onClick={()=>setImageOpen(!imageOpen)}/>
      </>
  )
}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 