
import axiosInstance from '../../helpers/axios';
import { useState, useEffect } from 'react';
import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Stack from '@mui/material/Stack';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import SettingsIcon from '@mui/icons-material/Settings';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import VideoLabelIcon from '@mui/icons-material/VideoLabel';
import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Box } from '@mui/material';

const CustomStepIcon = ({ index, consent ,cibil,status,selfie,digilocker,razorpayStaus,agreementStatus,disbursed,onClickLast }) => {
    let backgroundColor = '#898989'; 
    let cursorStyle = 'default';
  
    if (index === 0) {
        if (consent === "To Do") {
          backgroundColor = '#898989'; 
        } else if (consent) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 1) {
        if (cibil === "To Do") {
          backgroundColor = '#898989'; 
        } else if (cibil) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 2) {
        if (status === "To Do") {
          backgroundColor = '#898989'; 
        } else if (status === "On Hold" || status === "Application to be disbursed" || status === "Approved") {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 3) {
        if (selfie === "To Do") {
          backgroundColor = '#898989'; 
        } else if (selfie) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 4) {
        if (digilocker === "To Do") {
          backgroundColor = '#898989'; 
        } else if (digilocker) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 5) {
        if (razorpayStaus === "To Do") {
          backgroundColor = '#898989'; 
        } else if (razorpayStaus) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 6) {
        if (agreementStatus === "To Do") {
          backgroundColor = '#898989'; 
        } else if (agreementStatus) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 7) {
        if (disbursed === "To Do") {
          backgroundColor = '#898989'; 
        } else if (disbursed) {
          backgroundColor = '#12B76A'; 
          cursorStyle = 'pointer';
        } else {
          backgroundColor = '#D22129'; 
          cursorStyle = 'pointer';
        }
      }
  
      const handleClick = () => {
        if (index === 7 && onClickLast) {
          onClickLast();
        }
      };

    return (
      <div
        style={{
          width: 18,
          height: 18,
          borderRadius: '50%',
          backgroundColor,
          marginTop: '3px',cursor: cursorStyle,
        }} onClick={handleClick}
      >
      </div>
    );
  };

const CustomStepIconCoApp = ({ index, coAppconsent ,coAppCibil,status,coAppselfie,coAppdigilocker,coApprazorpayStaus,coAppAgeementStatus,disbursed}) => {
    let backgroundColor = '#898989'; 
  
    if (index === 0) {
        if (coAppconsent === "To Do") {
          backgroundColor = '#898989'; 
        } else if (coAppconsent) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 1) {
        if (coAppCibil === "To Do") {
          backgroundColor = '#898989'; 
        } else if (coAppCibil) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 2) {
        if (status === "To Do") {
          backgroundColor = '#898989'; 
        } else if (status === "On Hold" || status === "Application to be disbursed" || status === "Approved") {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
    
      if (index === 3) {
        if (coAppselfie === "To Do") {
          backgroundColor = '#898989'; 
        } else if (coAppselfie) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 4) {
        if (coAppdigilocker === "To Do") {
          backgroundColor = '#898989'; 
        } else if (coAppdigilocker) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 5) {
        if (coApprazorpayStaus === "To Do") {
          backgroundColor = '#898989'; 
        } else if (coApprazorpayStaus) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 6) {
        if (coAppAgeementStatus === "To Do") {
          backgroundColor = '#898989'; 
        } else if (coAppAgeementStatus) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
      if (index === 7) {
        if (disbursed === "To Do") {
          backgroundColor = '#898989'; 
        } else if (disbursed) {
          backgroundColor = '#12B76A'; 
        } else {
          backgroundColor = '#D22129'; 
        }
      }
    return (
      <div
        style={{
          width: 18,
          height: 18,
          borderRadius: '50%',
          backgroundColor,
          marginTop: '3px'
        }}
      >
      </div>
    );
  };

  
 

  
const ApplicationDetails = ({ applicationId,coappId })=>{
    
    const user = sessionStorage.getItem('user');
    const [agreementStatus,setAgreementStatus] = useState("")
    const [cibil,setCibil] = useState("")
    const [consent,setConsent] = useState("")
    const [digilocker,setDigilocker] = useState("")
    const [disbursed,setDisbursed] = useState("")
    const [razorpayStaus,setRazorPayStatus] = useState("")
    const [selfie,setSelfie] = useState("")
    const [status,setStatus] = useState("")
    const [coAppAgeementStatus,setCoAppAgreementStatus] = useState("")
    const [coAppCibil,setCoAppCibil] = useState("")
    const [coAppconsent,setCoAppConsent] = useState("")
    const [coAppdigilocker,setCoAppDigilocker] = useState("")
    const [coApprazorpayStaus,setCoAppRazorPayStatus] = useState("")
    const [coAppselfie,setCoAppSelfie] = useState("")

    const createUrl = `/audit/app-tracker`
    useEffect(()=>{
            axiosInstance.get(createUrl,{
                headers:{
                Authorization:`Bearer ${user}`
            }
            }).then((res)=>{
                setAgreementStatus(res?.data?.data?.agreementStatus)
                setCibil(res?.data?.data?.cibil)
                setConsent(res?.data?.data?.consent)
                setDigilocker(res?.data?.data?.digilocker)
                setDisbursed(res?.data?.data?.disbursed)
                setRazorPayStatus(res?.data?.data?.razorpayStatus)
                setSelfie(res?.data?.data?.selfie)
                setStatus(res?.data?.data?.status) 
                setCoAppAgreementStatus(res?.data?.data?.coapplicant?.agreementStatus)
                setCoAppCibil(res?.data?.data?.coapplicant?.cibil)
                setCoAppConsent(res?.data?.data?.coapplicant?.consent)
                setCoAppDigilocker(res?.data?.data?.coapplicant?.digilocker)
                setCoAppRazorPayStatus(res?.data?.data?.coapplicant?.razorpayStatus)
                setCoAppSelfie(res?.data?.data?.coapplicant?.selfie)
              
            
            }).catch((error)=>console.log("error",error))
        })
        const [isModalOpen, setIsModalOpen] = useState(false);
        const [applicantDetails,setApplicantDetails] = useState([])
  const handleLastStepClick = () => {
    console.log('Last step clicked!');
    setIsModalOpen(true);
    const urlUsers = `/admin/application/disbursed?applicationId=${applicationId}`
    axiosInstance.get(urlUsers,{
      headers:{
      Authorization:`Bearer ${user}`
  }
  }).then((res)=>{
      console.log("details",res?.data?.data?.details[0])
      setApplicantDetails(res?.data?.data?.details[0])
  
  }).catch((error)=>console.log("error",error))
  };
 
  



  const toggleModal = () => {
    // Close the modal
    setIsModalOpen(!isModalOpen);
  };
  function formatNumberWithCommas(number) {
    return number.toLocaleString('en-IN'); 
  }
const steps = ['Consent', 'Cibil & Income', 'Approved/Rejected','Selfie','Kyc','E Mandate','Agreement','Disbursement'];

    return(
        <Box sx={{ width: '88%' }} style={{marginLeft:'10%',backgroundColor:'#f0f0f0',borderRadius:'10px',paddingTop:'0.7em',boxShadow:'0px 0px 2px 0px rgb(0,0,0.2,0.6)'}}>
        {coappId?(
        <>
            <Stepper activeStep={-1} alternativeLabel >
            {steps.map((label, index) => (
              <Step key={label}>
                <StepLabel StepIconComponent={() => <CustomStepIcon index={index} consent={consent} cibil={cibil} status={status} selfie={selfie} digilocker={digilocker} razorpayStaus={razorpayStaus} agreementStatus={agreementStatus} disbursed={disbursed} onClickLast={handleLastStepClick}/>}>
                    {label}
                </StepLabel>
              </Step>
            ))}
            </Stepper>
            <Stepper activeStep={-1} alternativeLabel style={{paddingTop:'0.5em'}} >
            {steps.map((label, index) => (
              <Step key={label}>
              <StepLabel  StepIconComponent={() => <CustomStepIconCoApp index={index} coAppconsent={coAppconsent} coAppCibil={coAppCibil} status={status} coAppselfie={coAppselfie} coAppdigilocker={coAppdigilocker} coApprazorpayStaus={coApprazorpayStaus} coAppAgeementStatus={coAppAgeementStatus} disbursed={disbursed} />} style={{paddingBottom:'1em'}}></StepLabel>      </Step>
            ))}
            </Stepper>
        </>)
        :
        (<>
            <Stepper activeStep={-1} alternativeLabel style={{paddingBottom:'0.5em'}}>
            {steps.map((label, index) => (
              <Step key={label}>
                <StepLabel StepIconComponent={() => <CustomStepIcon index={index} consent={consent} cibil={cibil} status={status} selfie={selfie} digilocker={digilocker} razorpayStaus={razorpayStaus} agreementStatus={agreementStatus} disbursed={disbursed} onClickLast={handleLastStepClick}/>}>
                {label}
                </StepLabel>
              </Step>
            ))}
            </Stepper>
        </>)}
        <Modal isOpen={isModalOpen} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>Disbursement Details</ModalHeader>
        <ModalBody>
          {/* Your modal content goes here */}
         
            { applicantDetails?
            <>
             <div style={{display:"flex",flexDirection:"row",justifyContent:"space-between",backgroundColor:'#FCF0F1',width:"100%",borderRadius:"10px"}}>
              <div style={{marginLeft:"15px",fontFamily:"Inter-Medium",fontSize:"14px",color:"#D32028"}}>Application ID <br/><span style={{fontFamily:"Inter-Medium",fontSize:"14px",color:"black"}}>{applicantDetails?.applicationId}</span></div>
               <div style={{marginRight:"15px",fontFamily:"Inter-Medium",fontSize:"14px",color:"#D32028"}}> Applicant Name<br/><span style={{fontFamily:"Inter-Medium",fontSize:"14px",color:"black"}}>{applicantDetails?.name}</span></div>
            </div>
              <div style={{ display: 'flex', flexWrap: 'wrap',width:'100%' }}>
              {applicantDetails.coapplicantName? 
   <div style={{width:'50%',height:'60px', marginBottom: '10px'}}> 
   <p style={{display:'flex',flexDirection:"column",marginLeft:'5px'}}>
     <span style={{color:'gray',fontFamily:'Inter-Medium',fontWeight:'300',lineHeight:"20px",fontSize:'15px',marginTop:'20px'}}>Co-Applicant Name</span>
     
    <span 
         
         style={{
           
           textDecoration: 'none',
           color: '#232323',
            fontSize: '13px',
           fontFamily: 'Inter-Medium',
           lineHeight:"30px",
           display: 'block',
           width: '100%',
           wordBreak: 'break-word'
         }}>{applicantDetails?applicantDetails.coapplicantName:null}</span> 
   </p>
</div>:""}
   <div style={{width:'50%',height:'60px', marginBottom: '10px'}}> 
             <p style={{display:'flex',flexDirection:"column"}}>
               <span style={{color:'gray',fontFamily:'Inter-Medium',fontWeight:'300',lineHeight:"20px",fontSize:'15px',marginTop:'20px'}}>Subvention Amount</span>
               
              <span style={{color:"#232323",fontSize:'13px',fontFamily:'Inter-Medium',lineHeight:'30px'}}>{applicantDetails.subventionAmount ? ( <>&#8377;{formatNumberWithCommas(applicantDetails.subventionAmount)}</>) : ( '-')}</span> 
             </p>
   </div>
   <div style={{width:'50%',height:'60px', marginBottom: '10px'}}> 
             <p style={{display:'flex',flexDirection:"column",marginLeft:'5px'}}>
               <span style={{color:'gray',fontFamily:'Inter-Medium',fontWeight:'300',lineHeight:"20px",fontSize:'15px',marginTop:'20px'}}>Processing Fee</span>
               
              <span style={{color:"#232323",fontSize:'13px',fontFamily:'Inter-Medium',lineHeight:'30px'}}>{applicantDetails.processingFee ? ( <>&#8377;{formatNumberWithCommas(applicantDetails.processingFee)}</>) : ( '-')}</span> 
             </p>
   </div>
   <div style={{width:'50%',height:'60px', marginBottom: '10px'}}> 
             <p style={{display:'flex',flexDirection:"column"}}>
               <span style={{color:'gray',fontFamily:'Inter-Medium',fontWeight:'300',lineHeight:"20px",fontSize:'15px',marginTop:'20px'}}>Disbursed Amount</span>
               
              <span style={{color:"#232323",fontSize:'13px',fontFamily:'Inter-Medium',lineHeight:'30px'}}>{applicantDetails.disbursedAmount ? ( <>&#8377;{formatNumberWithCommas(applicantDetails.disbursedAmount)}</>) : ( '-')}</span> 
             </p>
   </div>
   <div style={{width:'50%',height:'60px', marginBottom: '10px'}}> 
             <p style={{display:'flex',flexDirection:"column",marginLeft:'5px'}}>
               <span style={{color:'gray',fontFamily:'Inter-Medium',fontWeight:'300',lineHeight:"30px",fontSize:'15px',marginTop:'20px'}}>Disbursement Utr</span>
               
              <span style={{color:"#232323",fontSize:'13px',fontFamily:'Inter-Medium',lineHeight:'30px'}}>{applicantDetails?applicantDetails.disbursementUtr:null}</span> 
             </p>
   </div>
   <div style={{width:'50%',height:'60px', marginBottom: '10px'}}> 
             <p style={{display:'flex',flexDirection:"column"}}>
               <span style={{color:'gray',fontFamily:'Inter-Medium',fontWeight:'300',lineHeight:"30px",fontSize:'15px',marginTop:'20px'}}>Disbursement Date</span>
               
              <span style={{color:"#232323",fontSize:'13px',fontFamily:'Inter-Medium',lineHeight:'30px'}}>{applicantDetails?applicantDetails.disbursementDate:null}</span> 
             </p>
   </div>
   
   </div>
        
 

        </>:<h3>Information is not available</h3>}
        </ModalBody>
       
      </Modal>
        </Box>
                    )
                }

export default ApplicationDetails

