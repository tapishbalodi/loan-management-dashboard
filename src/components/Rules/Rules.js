import React,{useRef,useState,useEffect} from 'react'
import {Card,Input} from 'reactstrap'; 
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
 import Sidebar from '../Sidebar';
 import Topbar from "../Topbar";
 import axiosInstance from '../../helpers/axios';
 import DataElements from './DataElements';
 import calender from "../../assets/images/calender.png"
 import Datetime from 'react-datetime';
 import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

const Rules = () => {
  const [openSources,setOpenSources]=useState(false)
  const [selectedSource, setSelectedSource] = useState(null); 
  const user = sessionStorage.getItem("user")
  const [sources,setSources]=useState([])
  const [dataElements,setDataElements]=useState([])
  const [clicked,setClicked] = useState(null)
  const [selectedElement, setSelectedElement] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedOperator, setSelectedOperator] = useState(null);
  const [name,setName] = useState(null);
  const [description,setDescription] = useState(null)
  const [id,setId] = useState(null)
  const [datatype,setDatatype] = useState(null)
  const [value1,setValue1] = useState(null)
  const [value2,setValue2] = useState(null)
  const [selectedOperatorText, setSelectedOperatorText] = useState("");

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const sourceUrl = `/rules/sources`
  useEffect(()=>{
    axiosInstance.get(sourceUrl, {
      headers: {
        Authorization: `Bearer ${user}`,
      },
    })
    .then((res) => {
        console.log("sources",res?.data?.data)
        setSources(res?.data?.data)
    })
    .catch((error) => {
      console.error("Error fetching filtered data:", error);
    });
  },[sourceUrl])
  const handleSourceHover = (sourceId) => {
    const dataElementsUrl = `/rules/data-elements?sourceId=${sourceId}`;
    axiosInstance
      .get(dataElementsUrl, {
        headers: {
          Authorization: `Bearer ${user}`,
        },
      })
      .then((res) => {
        console.log('Data Elements for sourceId',res?.data?.data);
        setDataElements(res?.data?.data)
      })
      .catch((error) => {
        console.error('Error fetching data elements:', error);
      });
      
  };
 
  const [selectedSourceId, setSelectedSourceId] = useState(null);

  const handleSourceSelect = (sourceKey) => {
    const selectedSourceId = sources[sourceKey];
    setSelectedSource(sourceKey);
    setSelectedSourceId(selectedSourceId);
    setOpenSources(false);
    setSelectedElement(null)

  };
  const handleDataElementSelect = (elementId,id,type) => {
    setSelectedElement(elementId);
    setId(id)
    setDatatype(type)
  };
  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };
  const ruleUrl = `/rules/create`
  const submit = ()=>{
    if (/\s/.test(name)) {
      alert("Spaces are not allowed in the Name");
      return; 
    }
    const data = {
        source:selectedSourceId,
        name:name,
        description:description,
        dataElement:id,
        operator:selectedOperator,
        value1:value1,
        value2:0
    };
    axiosInstance.post(ruleUrl,data,{
      headers:{
        Authorization:`Bearer ${user}`
    }
    }).then((res)=>{
      console.log("create rule",res?.data?.message)
      if(res?.data?.message === "Successful"){
        alert("Rule created Successful")
        setTimeout(() => window.location.reload(true), 1000);
      }
    }).catch((error)=>{
      alert(error?.response?.data?.message)
      console.log("error",error)
    })
  }
  const reset = ()=>{
    setSelectedElement(null)
    setSelectedOperator(null)
    setId(null)
    setSelectedOperatorText("")
  }
  const handleNameChange = (e) => {
    const inputValue = e.target.value;
  if (/\S/.test(inputValue)) {
    setName(inputValue);
  }else{
      alert("Spaces Are not allowed in the Name")
    }
  };
  const selectOperator = (selectedOperator,selectedOperatorText) =>{
    setSelectedOperator(selectedOperator);
    setSelectedOperatorText(selectedOperatorText);

  }
    return ( 
   <Card className="card" style={{width:"100%",height:'100%'}}>
      <Sidebar/>
      <div style={{width:'80%', overflow:"auto",marginLeft:"20%",padding:"10px",height:'100%'}}>
        <Topbar/>
        <div>            
            <p style={{color:'#D32027',paddingTop:'1em',fontFamily:'Inter-Medium',paddingLeft:'1em'}}>Rule Builder</p>
        </div>
        <div style={{display:'flex'}}>
        <div style={{width:'20%',marginLeft:'10px',boxShadow:'0px 2px 4px -2px #1018280F',borderRadius:'8px',border:'1px solid #EAECF0'}}>
          <p style={{textAlign:'center',fontFamily:'Inter-Medium',fontSize:'1vw'}}>Source</p>
          <ul>
          {Object.keys(sources).map((key) => (
              <li key={key} style={{fontFamily:'Inter-Medium',fontSize:'0.9vw',padding:'10px',cursor:"pointer",backgroundColor: clicked === key ? 'transparent' : 'transparent', // Check if it's clicked
              color: clicked === key ? '#D32028' : 'initial', }} onClick={() => {
                handleSourceHover(sources[key]);
                handleSourceSelect(key);
                setClicked(key); 
              }}>
              {key}
            </li>
            ))}
          </ul>
        </div>
        <Modal isOpen={isModalOpen} toggle={toggleModal} backdrop="static">
        <ModalHeader toggle={toggleModal}>Enter Details</ModalHeader>
        <ModalBody style={{padding:'20px'}}>
          <p style={{lineHeight:'1em',fontFamily:'Inter-Medium',fontSize:'0.9vw'}}>Rule Name</p>
          <Input onChange={handleNameChange} />
          <p style={{lineHeight:'1em',paddingTop:'1em',fontFamily:'Inter-Medium',fontSize:'0.9vw'}}>Rule Description</p>
          <Input onChange={(e)=>setDescription(e.target.value)}/>
        </ModalBody>
        <Button style={{backgroundColor:'#D32028',border:'none',width:'15%',display:'flex',justifyContent:'center',marginLeft:'42%',marginBottom:'15px',marginTop:'10px'}} onClick={submit}>
            Submit
          </Button>
        </Modal>
        {clicked ? (
          <div  style={{width:'78%',marginLeft:'10px',boxShadow:'0px 2px 4px -2px #1018280F',borderRadius:'8px',border:'1px solid #EAECF0'}}>  
          <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around'}}>
          <div style={{fontFamily:'Inter-Medium',fontSize:'0.9vw'}}>Source : <span style={{color:'#D32028',fontFamily:'Inter-Medium',fontSize:'0.9vw'}}>{selectedSource}</span> </div>
          <DataElements dataElements={dataElements} selectedElement={selectedElement}  onClick={handleDataElementSelect}/>
          </div>
          <div style={{display:'flex',flexDirection:'column',width:'100%'}}>
        
          {selectedOperator === "<>" ? 
          <div style={{display:'flex',alignItems:'center',justifyContent:'center',padding:'1em',border:'1px solid #EAECF0',height:'15vh',marginTop:'3em',width:"90%",marginLeft:'5%', borderRadius: "8px"}}>
          <div style={{fontFamily:'Inter-Medium',fontSize:'1vw',marginRight:'2%'}}>{selectedElement}</div>
          <div style={{fontFamily:'Inter-Medium',fontSize:'1.9vw'}}>{">"}</div>
        {selectedOperator ? <div ><Input style={{marginLeft:'4%',width:'60%',marginTop:'5px'}} onChange={(e)=>setValue1(e.target.value)} value={value1}/></div> : ''}  
        <div style={{fontFamily:'Inter-Medium',fontSize:'1.9vw'}}>{"<"}</div>
        {selectedOperator ? <div ><Input style={{marginLeft:'4%',width:'60%',marginTop:'5px'}} onChange={(e)=>setValue2(e.target.value)} value={value2}/></div> : ''}  
        </div>
        :
        <div style={{display:'flex',alignItems:'center',justifyContent:'center',padding:'1em',border:'1px solid #EAECF0',height:'15vh',marginTop:'3em',width:"90%",marginLeft:'5%', borderRadius: "8px"}}>
        <div style={{fontFamily:'Inter-Medium',fontSize:'1vw',marginRight:'2%'}}>{selectedElement}</div>
    {selectedElement ? <>{datatype === 1 ? <Input style={{marginLeft:'1%',width:'20%',marginTop:'5px'}}  onChange={(e)=>setValue1(e.target.value)} value={value1}/> : datatype === 2 ? <div ><Input style={{marginLeft:'4%',width:'100%',marginTop:'5px'}} onChange={(e)=>setValue1(e.target.value)} value={value1}/></div> : <Datetime
                closeOnSelect="false"
                id="fromDate"
                dateFormat="DD MMM YYYY"
                timeFormat={false}
                inputProps={{ placeholder: 'Select Date',style:{fontFamily:'Inter-Medium', backgroundImage: `url(${calender})`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: '5px center',
                backgroundSize: '15px 15px', paddingLeft: '30px',fontSize:"14px",width:'200px'} }}
              />}</>  : ''}
      </div>
        }
        {clicked ? 
        <div style={{display:'flex',justifyContent:'flex-end',margin:'2%',paddingRight:'3%'}}>
        <button  style={{backgroundColor:'#D32028',borderRadius:'8px',paddingLeft:'10px',height:'40px',paddingRight:'10px',fontFamily:'Inter-Medium',color:'white',    cursor: 'pointer' }}
         onClick={() => {
            toggleModal();
        }}>Save</button>
        <button style={{backgroundColor:'#898989',borderRadius:'8px',paddingLeft:'10px',height:'40px',paddingRight:'10px',fontFamily:'Inter-Medium',color:'white',marginLeft:'15%'}} onClick={reset}>Reset</button>
        </div>:""}
         
         </div>
        </div>
       ):"" }
        </div>
      </div>
   </Card>
);
};

export default Rules;







