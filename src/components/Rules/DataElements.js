
import React, { useState } from 'react';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import "./Rules.css"
const DataElements = ({ dataElements,selectedElement, onClick  }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);
  return (
    <div>
    <Dropdown isOpen={dropdownOpen} toggle={toggle} >
      <DropdownToggle caret style={{backgroundColor:'#D32028',border:'none'}}>{selectedElement ? selectedElement : "Select Data Element"}</DropdownToggle>
      <DropdownMenu>
       <DropdownItem style={{backgroundColor:'white'}}>
       {dataElements.map((element) => (
        <div key={element.id} onClick={() => onClick(element.element,element.id,element.datatype)} style={{ fontFamily:'Inter-Medium',fontSize:'0.9vw',cursor: 'pointer',  color: selectedElement === element.element ? '#D32028' : 'initial' ,marginLeft:'10px'}}>
          {element.element}
        </div>
      ))}
       </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  </div>
   
   
  );
};

export default DataElements;