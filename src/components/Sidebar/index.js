import React,{useState,useEffect} from "react";
import Logo from "../../assets/images/svg/Logo.svg";
import { Homestyle } from "./Style";
import jwt_decode from 'jwt-decode';
import repayment from "../../assets/images/repayment.png"
import document from "../../assets/images/document.png"
import institute from "../../assets/images/institute.png"
import emis from "../../assets/images/createmanual.png"
import overdue from "../../assets/images/overdue.png"
// import dropdown from "../../assets/images/dropdown.png";
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import dropdown from '@mui/icons-material/KeyboardArrowRight';
import dashboardIcon from "../../assets/images/dashboardIcon.png";
import homeIcon from "../../assets/images/home.png";
import userSearch from "../../assets/images/userSearch.png";
import roles from "../../assets/images/roles.png";
import box from "../../assets/images/box.png";
import loans from "../../assets/images/loans.png";
import statusIcon from "../../assets/images/statusIcon.png";
import dropup from "../../assets/images/dropup.png";
import 'bootstrap/dist/css/bootstrap.css';
import OverduetTable from "../Overdue/Overdue";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { Link, useNavigate } from "react-router-dom";
import axiosInstance from "../../helpers/axios";
import "./index.css"

import {
  Box,
  TextField,
  InputAdornment,
  List,
  ListItem,
  Button,
  Typography,
  Card,
} from "@mui/material";


import { useLocation } from "react-router-dom";
import { event } from "jquery";


export default function Sidebar({isOpenSidebar,handler}) {
  const [isOpen, setIsOpen] = useState(true);
  const [isOpen1, setIsOpen1] = useState(false);
  const [isOpenHome, setIsOpenHome] = useState(false);
  const [dropdownItems, setDropdownItems] = useState([]);


  const user = sessionStorage.getItem("user")
  const url =`/admin/application/count`;
  const decode=jwt_decode(user)

  const [isClicked, setIsClicked] = useState(false);
  const toggleDropdown = () => {
    setIsClicked(!isClicked);
    setIsOpen(!isOpen);
    
  };

  const [isClick, setIsClick] = useState(false);
  const toggleDropdown1 = () => {
    setIsClick(!isClick);
    setIsOpen1(!isOpen1);  
  };
  
  const toggleDropdownHome = () => {
    setIsOpenHome(!isOpenHome);
    
  };

  
  const location = useLocation();
  const navigate = useNavigate()
  
  useEffect(() => {

 
    axiosInstance
      .get(url,{
        headers: {
          Authorization: `Bearer ${user}`,
        }
      })
      .then((response) => {
    
        setDropdownItems(response.data.data)
        
      }
      )
      .catch((error) => console.error(error));
  }, [url,user]);
   
      const handleNavigate = (path)=>{
        sessionStorage.removeItem("page")
        window.location=path
      }
   
  
  const filteredDropdownItems = dropdownItems.filter((_, index) => index !== 1 );
  const [sidebarOpen,setSidebarOpen] = useState(isOpenSidebar)
  
  const handlerFunction=()=>{
    handler(!sidebarOpen)
    setSidebarOpen(!sidebarOpen)
  }



  return (
    <>
    {sidebarOpen === true ? (
      <>
       <Homestyle className="Home" style={{width : '20%',boxShadow:'0px 8px 10px rgba(3, 3, 3, 0.28)'}}>
      <ArrowBackIosIcon style={{marginLeft:'95%',cursor:'pointer'}} onClick={()=>handlerFunction()}/>

        <div className="paper-setting"  style={{marginTop:'-2rem'}}>
          <Box>
            <Link to="/">
              <Box component="img" alt="" src={Logo} />
            </Link>
          </Box>
          <Box my={10}>
            {/* <TextField
              className="searchiput"
              id="outlined-basic"
              placeholder="Search"
              variant="outlined"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />{" "}
                  </InputAdornment>
                ),
              }}
            /> */}
          </Box>
          <List className="list-man-nav">
          <Link to="/usersearch" className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/usersearch"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={userSearch} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        User Search
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </Link>
           
            <Link onClick={()=>toggleDropdownHome()} className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary"    fullWidth size="small"  style={{backgroundColor:isOpenHome?'#ffedee':""}}>
             
                  {/* inner List started */}
                  <List className=" List-main-inner " >
                    <ListItem>
                      <img style={{width:'20px'}} src={homeIcon} alt="student"/>
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography variant="body1" gutterBottom>
                      <div className="dropdown-button" style={{display:'flex',justifyContent: 'space-between',width:"10.5vw"}} >
                        <span >Dashboard</span>
                        <ListItem>
                      {isOpenHome ? (  <ExpandMoreIcon  style={{ width: '25px', height: '30px', marginLeft: '2em' }}/> ) : (
                        <KeyboardArrowRightIcon style={{ width: '25px', height: '30px', marginLeft: '2em' }} />)}
                    </ListItem>
                        </div>
                      </Typography>
                    </ListItem>  
                    
                  </List>
                 
                </Button>
                {/* inner List ended */}
              </ListItem>
            </Link>
            <div className="sidebar-data-open">
      {isOpenHome && (
  <div style={{width:'15vw',
  display: 'flex',
  flexDirection: 'column',backgroundColor:'#FFFCFC',borderBottomLeftRadius:"10px",borderBottomRightRadius:"10px",borderBottom:'2px solid #D32028',
  borderTop:'2px solid #D32028'
  
 }}>
      
      <a href="/dashboard"   className="hover-effect"  style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',marginTop:'10px'}} >
        <div   style={{display: 'flex',alignItems:"center", flexDirection: 'row',justifyContent: 'space-between', lineHeight:'30px'}}>
          <p id="InSubmission" style={{display: 'inline',whiteSpace:'nowrap',color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[1] === "dashboard" ? "underline" : ""}}>Apply Now</p>
        {/* {decode.role!=="institute_user"&&decode.role!=="institute_admin" ? <p  id="InSubmissionCount" className={location.pathname.split('/')[2]==="Submission"?"count":"count-select"} >{dropdownItems.find((item) => item.status === 'In Submission')?.count || 0}</p> : "" }   */}
        </div>
      </a>
      { decode.role!=="institute_user"&&decode?.role!=="institute_admin" ? <a href="/analytics"    className="hover-effect"  style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="Analytics" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[1] === "analytics" ? "underline" : ""}}>Analytics</p>
        {/* {decode.role!=="institute_user"&&decode.role!=="institute_admin" ? <p  id="InReviewCount" className={location.pathname.split('/')[2]==="Review"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'In Review')?.count || 0}</p>:''}   */}
        </div>
      </a>:null}
      { decode.role!=="institute_user"&&decode?.role!=="institute_admin" ? <a href="/DemandReport"    className="hover-effect"  style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="DemandReport" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[1] === "DemandReport" ? "underline" : ""}}>Demand Reports</p>
        {/* {decode.role!=="institute_user"&&decode.role!=="institute_admin" ? <p  id="InReviewCount" className={location.pathname.split('/')[2]==="Review"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'In Review')?.count || 0}</p>:''}   */}
        </div>
      </a>:null}
      
  
  </div>
      )}
      
      
     
    </div>
      
            {/* {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? 
            <>
            <Link to="/analytics" className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/analytics"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  inner List started
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={dashboardIcon} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Analytics
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                inner List ended
              </ListItem>
            </Link>
            </>:""}
             */}
             <>
           
              <div>
            <Link className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/feeCycles" ? "list-top" : "list-top-2"
                }`}
              >
                <Button color="primary"  size="small" fullWidth   onClick={toggleDropdown}  style={{ backgroundColor: isClicked ? '' : '#ffedee' ,textTransform: 'none'}}>
                  {/* inner List started */}
                  
                  <List className=" List-main-inner" >
                    <ListItem>
                       <img style={{width:'20px'}} src={statusIcon} alt="fee cycle"/>
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1" gutterBottom>
                          
                        <div className="dropdown-button" style={{display:'flex',justifyContent: 'space-between',width:"10.5vw"}} >
                          <p style={{fontFamily:'Inter-Medium',paddingTop:'10px'}}>Status</p>
                           <ListItem>
                      {isOpen? (  <ExpandMoreIcon  style={{ width: '25px', height: '30px', marginLeft: '3.5em' }}/> ) : (
                        <KeyboardArrowRightIcon style={{ width: '25px', height: '30px', marginLeft: '3.5em'  }} />)}
                    </ListItem>
                      </div>
                      </Typography>
                    </ListItem>
                    
                  </List>
                </Button>
                
                {/* inner List ended */}
              </ListItem>

            </Link>
        <div className="sidebar-data-open">
      {isOpen && (
  <div style={{width:'15vw',
  display: 'flex',
  flexDirection: 'column',backgroundColor:'#FFFCFC',borderBottomLeftRadius:"10px",borderBottomRightRadius:"10px",borderBottom:'2px solid #D32028',
  borderTop:'2px solid #D32028'
  
 }}>
      
      <a  onClick={()=>handleNavigate("/status/Submission")}  className="hover-effect"  style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',marginTop:'10px',cursor:'pointer'}} >
        <div   style={{display: 'flex',alignItems:"center", flexDirection: 'row',justifyContent: 'space-between', lineHeight:'30px'}}>
          <p id="InSubmission"  style={{display: 'inline',whiteSpace:'nowrap',color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Submission" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Submission" ? "600" : ""}}>In Submission</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p  id="InSubmissionCount" className={location.pathname.split('/')[2]==="Submission"?"count":"count-select"} >{dropdownItems.find((item) => item.status === 'In Submission')?.count || 0}</p> : "" }  
        </div>
      </a>
      <a onClick={()=>handleNavigate("/status/Review")}  className="hover-effect"  style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="InReview" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Review" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Review" ? "600" : ""}}>In Review</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p  id="InReviewCount" className={location.pathname.split('/')[2]==="Review"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'In Review')?.count || 0}</p>:''}  
        </div>
      </a>
      <a  onClick={()=>handleNavigate("/status/Process")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="InProcess" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Process" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Process" ? "600" : ""}}>In Process</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p id="InProcessCount"  className={location.pathname.split('/')[2]==="Process"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'In Process')?.count || 0}</p>:''}  
        </div>
      </a>
      <a onClick={()=>handleNavigate("/status/Sanction")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="InSanction" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Sanction" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Sanction" ? "600" : ""}}>In Sanction</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p  id="InSanctionCount" className={location.pathname.split('/')[2]==="Sanction"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'In Sanction')?.count || 0}</p>:''}  
        </div>
      </a>
      <a  onClick={()=>handleNavigate("/status/Disbursement")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="InDisbursement" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Disbursement" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Disbursement" ? "600" : ""}}>In Disbursement</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p  id="InDisbursementCount"  className={location.pathname.split('/')[2]==="Disbursement"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'Ready to Disburse')?.count || 0}</p>:''}  
        </div>
      </a>
      {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ?   <a  onClick={()=>handleNavigate("/status/Pending")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="Pending" style={{display: 'inline',whiteSpace:'nowrap',color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Pending" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Pending" ? "600" : ""}}>Pending Approvals</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p id="PendingCount"   className={location.pathname.split('/')[2]==="Pending"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'Pending Disbursement')?.count || 0}</p>:''}  
        </div>
      </a>:"" }
   
      <a onClick={()=>handleNavigate("/status/Disbursed")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p  id="InDisbursed" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Disbursed" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Disbursed" ? "600" : ""}}>Disbursed</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p id="InDisbursedCount"  className={location.pathname.split('/')[2]==="Disbursed"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'Disbursed')?.count || 0}</p>:''}  
        </div>
      </a>
      <a  onClick={()=>handleNavigate("/status/Dropped")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p  id="Dropped"  style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Dropped" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Dropped" ? "600" : ""}}>Dropped</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p id="DroppedCount"  className={location.pathname.split('/')[2]==="Dropped"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'Dropped')?.count || 0}</p>:''}  
        </div>
      </a>
      <a  onClick={()=>handleNavigate("/status/Rejected")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="Rejected" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Rejected" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Rejected" ? "600" : ""}}>Rejected</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p id="RejectedCount" className={location.pathname.split('/')[2]==="Rejected"?"count":"count-select"}>{dropdownItems.find((item) => item.status === 'Rejected')?.count || 0}</p>:''}  
        </div>
      </a>
      <a  onClick={()=>handleNavigate("/status/Hold")} className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',cursor:'pointer'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="onHold" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem",textDecoration: location.pathname.split('/')[2] === "Hold" ? "underline" : "",fontWeight:location.pathname.split('/')[2] === "Hold" ? "600" : ""}}>On Hold</p>
        {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <p  id="onHoldCount" className={location.pathname.split('/')[2]==="Hold"?"count":"count-select"} >{dropdownItems.find((item) => item.status === 'On Hold')?.count || 0}</p>:''}  
        </div>
      </a>
      
  
  </div>
      )}
      
      
     
    </div>
      
            </div>
          
             </> 
          {decode.role !=="institute_user" && decode.role!=="institute_admin" ?
          <>
            <div    >
              <div>
              <Link  className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/applications1"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small"  onClick={toggleDropdown1} style={{  backgroundColor: isClick ? '#ffedee' : '',  textTransform: 'none'   }} >
                  {/* inner List started */}
                 
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={statusIcon} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                           
                           <div className="dropdown-button" style={{display:'flex',justifyContent: 'space-between',width:"10.5vw"}} >
                          <p style={{fontFamily:'Inter-Medium',paddingTop:'10px'}}>Applications</p>
                          <ListItem>
                      {isOpen1 ? (  <ExpandMoreIcon  style={{ width: '25px', height: '30px', marginLeft: '1.5em' }}/> ) : (
                        <KeyboardArrowRightIcon style={{ width: '25px', height: '30px', marginLeft: '1.5em' }} />)}
                    </ListItem>
                      </div>
                      </Typography>
                    </ListItem>
                    
                  </List>
                 
                </Button>
                {/* inner List ended */}
              </ListItem>
            </Link>
        <div className="sidebar-data-open">
      {isOpen1 && (
  <div style={{width:'15vw',
  display: 'flex',
  flexDirection: 'column',backgroundColor:'#FFFCFC',borderBottomLeftRadius:"10px",borderBottomRightRadius:"10px",borderBottom:'2px solid #D32028',
  borderTop:'2px solid #D32028'
  
 }}>
      
     
     
      <a href="/applications"  className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px',marginTop:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="InProcess" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem"}}>In Process</p>
        </div>
      </a>
      <a href="/status/Disbursed" className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p  id="InDisbursed" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem"}}>Disbursed</p>
        </div>
      </a>
      <a href="/status/Dropped" className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p  id="Dropped"  style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem"}}>Dropped</p>
        </div>
      </a>
      <a href="/status/Rejected"  className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="Rejected" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem"}}>Rejected</p>
        </div>
      </a>
      <a href="/status/Hold"  className="hover-effect" style={{textDecoration:'none',paddingLeft:'20px',paddingRight:'10px'}}>
        <div style={{width:"100%",display: 'flex', flexDirection: 'row', justifyContent: 'space-between',lineHeight:'30px'}}>
          <p id="onHold" style={{display: 'inline',whiteSpace:"nowrap",color:"black",fontFamily:'Inter-Medium',fontSize:"1rem"}}>On Hold</p>
        </div>
      </a>
      
  
  </div>
      )}
      
      
     
    </div>
      
            </div>
          </div>
          </> :""}
            
            
      
           
            {decode.role!=="institute_user"&&decode.role!=="institute_admin"&&decode?.role!=="institute_admin" ? <>
            <a href = "/Loans" className="linkdco" >
              <ListItem
                className={`${
                  location.pathname === "/Loans"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={loans} alt="loans" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Loans
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a  >

            <a href ="/Repayments"  className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/Repayments"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={repayment} alt="loans" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Repayments
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a >
            <a href ="/Emis"  className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/Emis"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={emis} alt="loans" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        EMI's
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a >
            <a href ="/Overdue"  className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/Overdue"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={overdue} alt="loans" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Overdue
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a >
            <a href ="/products" className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/products"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={box} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Products
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a>
            <a href ="/Institute" className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/Institute"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={institute} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Institutes
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a >
            </>:''}
            <a href ="/reports" className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/reports"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'20px'}} src={document} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Reports
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a>
          { decode?.role==="institute_admin"||decode?.role==="admin" ? <a href ="/roles" className="linkdco">
              <ListItem
                className={`${
                  location.pathname === "/roles"
                    ? "list-top"
                    : "list-top-2"
                }`}
              >
                <Button color="primary" fullWidth size="small">
                  {/* inner List started */}
                  <List className=" List-main-inner ">
                    <ListItem>
                      <img style={{width:'35px',marginLeft:'-12px'}} src={roles} alt="search" />
                    </ListItem>
                    <ListItem className="user-setting">
                      <Typography
                        variant="body1"
                        fontFamily="Inter-Medium"
                        gutterBottom
                      >
                        Roles
                      </Typography>
                    </ListItem>
                  </List>
                </Button>
                {/* inner List ended */}
              </ListItem>
            </a>:''}
           </List>
          {/* {profileDisplay!="100%"?<Card className="nav-card">
            <Box component="img" alt="" src={Ring} />
            <Typography
              variant="body2"
              color="warning.main"
              fontFamily="Inter-Medium"
              className="ring-text"
              gutterBottom
            >
              Profile Status
            </Typography>
            <Typography
              variant="body2"
              color="primary.dark"
              fontFamily="Inter-Medium"
              className="ring-text"
              gutterBottom
            >
              You have finished {profileDisplay} of your profile. Please{" "}
              <span className="click-here" onClick={() => navigate('/myprofile')}>click here </span> to complete
            </Typography>
          </Card>:null} */}
          
        </div>
      </Homestyle>
      </>
    ):<ArrowForwardIosIcon onClick={()=>{handlerFunction()}} style={{cursor:'pointer'}}/>}
     

    </>
  );
}