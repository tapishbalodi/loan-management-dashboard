import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Handleapproveapifund(state = initialState, action) {
  
  console.log("inside handleapproveapi", action)
  switch (action.type) {
    case userConstants.HANDLEAPPROVEAPIFUND_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.HANDLEAPPROVEAPIFUND_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.HANDLEAPPROVEAPIFUND_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}