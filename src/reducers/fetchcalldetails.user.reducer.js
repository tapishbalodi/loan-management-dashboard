import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Calldetails(state = initialState, action) {
  
  console.log("inside Inrejected", action)
  switch (action.type) {
    case userConstants.CALLDETAILS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.CALLDETAILS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.CALLDETAILS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}