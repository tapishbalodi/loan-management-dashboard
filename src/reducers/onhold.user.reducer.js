import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Onhold(state = initialState, action) {
  
  console.log("inside Onhold", action)
  switch (action.type) {
    case userConstants.ONHOLD_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.ONHOLD_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.ONHOLD_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}