import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Readytodisburse(state = initialState, action) {
  
  console.log("inside Readytodisburse", action)
  switch (action.type) {
    case userConstants.READYTODISBURSE_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.READYTODISBURSE_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.READYTODISBURSE_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}