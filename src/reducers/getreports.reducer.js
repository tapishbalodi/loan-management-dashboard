import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Reports(state = initialState, action) {
  
  console.log("inside reports", action)
  switch (action.type) {
    case userConstants.GET_REPORTS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.GET_REPORTS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.GET_REPORTS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}