import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function POSTACTIVITY(state = initialState, action) {
  
  console.log("inside submit activity function", action)
  switch (action.type) {
    case userConstants.POST_ACTIVITY_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.POST_ACTIVITY_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.POST_ACTIVITY_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}