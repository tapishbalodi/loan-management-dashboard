import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function callBankdetails(state = initialState, action) {
  
  console.log("inside callbankdetails function", action)
  switch (action.type) {
    case userConstants.CALLBANKDETAILS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.CALLBANKDETAILS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.CALLBANKDETAILS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}