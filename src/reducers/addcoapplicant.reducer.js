import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Addcoapplicant(state = initialState, action) {
  
  //console.log("datesssssssfilter", action)
  switch (action.type) {
    case userConstants.ADDCOAPPLICANT_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.ADDCOAPPLICANT_SUCCESS:
      const {res} = action
        console.log("dateAction", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.ADDCOAPPLICANT_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}