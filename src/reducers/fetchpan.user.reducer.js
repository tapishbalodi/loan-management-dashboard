import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Pandetails(state = initialState, action) {
  
  console.log("inside pan", action)
  switch (action.type) {
    case userConstants.PAN_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.PAN_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.PAN_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}