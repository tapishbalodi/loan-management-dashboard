import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Handleapproveapi(state = initialState, action) {
  
  console.log("inside handleapproveapi function", action)
  switch (action.type) {
    case userConstants.HANDLEAPPROVEAPI_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.HANDLEAPPROVEAPI_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.HANDLEAPPROVEAPI_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}