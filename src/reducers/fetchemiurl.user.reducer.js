import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Emidetails(state = initialState, action) {
  
  console.log("inside Applicationdetails function", action)
  switch (action.type) {
    case userConstants.EMIURL_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.EMIURL_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.EMIURL_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}