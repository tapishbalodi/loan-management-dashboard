import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Filteredusers(state = initialState, action) {
  
  console.log("inside filtered function", action)
  switch (action.type) {
    case userConstants.FILTEREDUSERS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.FILTEREDUSERS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.FILTEREDUSERS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}