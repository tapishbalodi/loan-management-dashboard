import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Bankselfiedetails(state = initialState, action) {
  
  console.log("inside Bankselfiedetails function", action)
  switch (action.type) {
    case userConstants.BANKSELFIE_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.BANKSELFIE_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.BANKSELFIE_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}