import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Updatestatus(state = initialState, action) {
  
  console.log("inside updatestatus url", action)
  switch (action.type) {
    case userConstants.UPDATESTATUSCHANGE_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.UPDATESTATUSCHANGE_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.UPDATESTATUSCHANGE_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}