import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Institutes(state = initialState, action) {
  
  console.log("inside Institutes function", action)
  switch (action.type) {
    case userConstants.INSTITUTES_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.INSTITUTES_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.INSTITUTES_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}