import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Handleapprove(state = initialState, action) {
  
  console.log("inside handleapprove function", action)
  switch (action.type) {
    case userConstants.HANDLEAPPROVEFORM_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.HANDLEAPPROVEFORM_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.HANDLEAPPROVEFORM_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}