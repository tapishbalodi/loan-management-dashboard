import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Indisbursed(state = initialState, action) {
  
  console.log("inside indisubursed function", action)
  switch (action.type) {
    case userConstants.INDISBURSED_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.INDISBURSED_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.INDISBURSED_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}