import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Repayments(state = initialState, action) {
  
  console.log("inside repayments function", action)
  switch (action.type) {
    case userConstants.REPAYMENTS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.REPAYMENTS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.REPAYMENTS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}