import { userConstants } from '../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function LoanDetails(state = initialState, action) {
  
  console.log("inside Loansdetails function", action)
  switch (action.type) {
    case userConstants.LOANDETAILS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.LOANDETAILS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.LOANDETAILS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}