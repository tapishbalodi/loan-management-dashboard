import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Disbursedstatuschange(state = initialState, action) {
  
  console.log("inside disbursedstatuschange function", action)
  switch (action.type) {
    case userConstants.DISBURSERDSTATUSCHANGE_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.DISBURSERDSTATUSCHANGE_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.DISBURSERDSTATUSCHANGE_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}