import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Droppedstatuschange(state = initialState, action) {
  
  console.log("inside droppedstatuschange function", action)
  switch (action.type) {
    case userConstants.DROPPEDSTATUSCHANGE_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.DROPPEDSTATUSCHANGE_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.DROPPEDSTATUSCHANGE_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}