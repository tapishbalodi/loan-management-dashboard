import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Pendingindisburse(state = initialState, action) {
  
  console.log("inside pendingdisburse function", action)
  switch (action.type) {
    case userConstants.PENDINGINDISBURSMENT_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.PENDINGINDISBURSMENT_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.PENDINGINDISBURSMENT_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}