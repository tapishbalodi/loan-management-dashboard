import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function callRegdetails(state = initialState, action) {
  
  console.log("inside regdetails function", action)
  switch (action.type) {
    case userConstants.CALLREG_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.CALLREG_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.CALLREG_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}