import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Getaadharback(state = initialState, action) {
  
  //console.log("inside fundcode", action)
  switch (action.type) {
    case userConstants.GETAADHARBACK_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.GETAADHARBACK_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.GETAADHARBACK_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}