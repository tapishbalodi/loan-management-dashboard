import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function ColenderFundSource(state = initialState, action) {
  
  console.log("inside handleapprove function", action)
  switch (action.type) {
    case userConstants.HANDLECOLENDERFUND_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.HANDLECOLENDERFUND_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.HANDLECOLENDERFUND_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}