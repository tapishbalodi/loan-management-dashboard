import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Loans(state = initialState, action) {
  
  console.log("inside Loans function", action)
  switch (action.type) {
    case userConstants.LOANS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.LOANS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.LOANS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}