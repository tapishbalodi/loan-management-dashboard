import {userConstants} from '../constants/user.constants'

const user=sessionStorage.getItem('user')
const initialState=user? {loggedIn:true,isLoading:false,user }:{}

export function getNotifications(state = initialState, action) {
  
  console.log("inside getNotifications function", action)
  switch (action.type) {
    case userConstants.GET_NOTIFICATIONS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.GET_NOTIFICATIONS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.GET_NOTIFICATIONS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}
export function deleteReminder(state = initialState, action) {
  
  console.log("inside delete function", action)
  switch (action.type) {
    case userConstants.DELETE_REMINDER_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.DELETE_REMINDER_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.DELETE_REMINDER_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}