import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Reviewstatuschange(state = initialState, action) {
  
  console.log("inside reviewstatuschange function", action)
  switch (action.type) {
    case userConstants.REVIEWSTATUSCHANGE_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.REVIEWSTATUSCHANGE_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.REVIEWSTATUSCHANGE_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}