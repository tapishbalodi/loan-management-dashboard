import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Handleuser(state = initialState, action) {
  
  console.log("inside handleuser function", action)
  switch (action.type) {
    case userConstants.HANDLEUSER_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.HANDLEUSER_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.HANDLEUSER_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}