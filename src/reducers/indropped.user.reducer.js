import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Indropped(state = initialState, action) {
  
  console.log("inside dropped", action)
  switch (action.type) {
    case userConstants.DROPPED_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.DROPPED_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.DROPPED_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}