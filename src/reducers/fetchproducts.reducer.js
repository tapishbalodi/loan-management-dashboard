import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Products(state = initialState, action) {
  
  console.log("inside Institutes function", action)
  switch (action.type) {
    case userConstants.PRODUCTS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.PRODUCTS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.PRODUCTS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}