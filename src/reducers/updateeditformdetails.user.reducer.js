import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Updateeditform(state = initialState, action) {
  
  console.log("inside Updateeditform url", action)
  switch (action.type) {
    case userConstants.UPDATEEDITFORM_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.UPDATEEDITFORM_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.UPDATEEDITFORM_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}