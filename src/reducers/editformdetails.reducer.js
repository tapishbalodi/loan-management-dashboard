import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Editform(state = initialState, action) {
  
  console.log("inside editform function", action)
  switch (action.type) {
    case userConstants.EDITFORMDETAILS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.EDITFORMDETAILS_SUCCESS:
      const {res} = action
        console.log("edit form Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.EDITFORMDETAILS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}