import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Inreview(state = initialState, action) {
  
  console.log("inside Inreview", action)
  switch (action.type) {
    case userConstants.INREVIEW_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.INREVIEW_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.INREVIEW_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}