import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function createInst(state = initialState, action) {
  
  console.log("inside create institute function", action)
  switch (action.type) {
    case userConstants.CREATEINST_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.CREATEINST_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.CREATEINST_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}