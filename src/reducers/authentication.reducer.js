import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { loggedIn: false, user, isLoading: false } : {};

export function authentication(state = initialState, action) {
  console.log("inside authentication function", action)
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user,
        isLoading : true

      };
    case userConstants.LOGIN_SUCCESS:
      console.log("staaaa",action)
      return {
        loggedIn: true,
        user: action.user,
        isLoading : false
      };
    case userConstants.LOGIN_FAILURE:
      return {
        isLoading : false
      };
    case userConstants.LOGOUT:
      return {};
    default:
      return state
  }
}
export function authenticationOtp(state = initialState, action) {
  console.log("inside authentication with Otp function", action)
  switch (action.type) {
    case userConstants.LOGIN_OTP_REQUEST:
      return {
        loggingIn: true,
        user: action.user,
        isLoading : true

      };
    case userConstants.LOGIN_OTP_SUCCESS:
      console.log("staaaa",action)
      return {
        loggedIn: true,
        user: action.user,
        isLoading : false
      };
    case userConstants.LOGIN_OTP_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}
export function sendOtp(state = initialState, action) {
  console.log("inside send Otp function", action)
  switch (action.type) {
    case userConstants.SEND_OTP_REQUEST:
      return {
        loggingIn: true,
        user: action.user,
        isLoading : true

      };
    case userConstants.SEND_OTP_SUCCESS:
      console.log("staaaa",action)
      return {
        loggedIn: true,
        user: action.user,
        isLoading : false
      };
    case userConstants.SEND_OTP_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}
