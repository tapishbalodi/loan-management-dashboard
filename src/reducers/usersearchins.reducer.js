import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function UserSearchIns(state = initialState, action) {
  
  console.log("inside usersearchins function", action)
  switch (action.type) {
    case userConstants.USERSEARCHINS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.USERSEARCHINS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.USERSEARCHINS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}