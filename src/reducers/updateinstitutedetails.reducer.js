import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function InstitueDetails(state = initialState, action) {
  
  console.log("inside bankingdetails function", action)
  switch (action.type) {
    case userConstants.UPDATEINSTITUTEDETAILS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.UPDATEINSTITUTEDETAILS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.UPDATEINSTITUTEDETAILS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}