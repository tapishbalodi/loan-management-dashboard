import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Applicationdetails(state = initialState, action) {
  
  console.log("inside Applicationdetails function", action)
  switch (action.type) {
    case userConstants.APPLICATIONDETAILS_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.APPLICATIONDETAILS_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.APPLICATIONDETAILS_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}