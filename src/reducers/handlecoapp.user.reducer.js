import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function Handlecoappuser(state = initialState, action) {
  
  console.log("inside handlecoappuser function", action)
  switch (action.type) {
    case userConstants.HANDLECOAPPUSER_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.HANDLECOAPPUSER_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.HANDLECOAPPUSER_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}