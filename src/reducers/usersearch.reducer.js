import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function UserSearch(state = initialState, action) {
  
  console.log("inside Insubmission function", action)
  switch (action.type) {
    case userConstants.USERSEARCH_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.USERSEARCH_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.USERSEARCH_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}