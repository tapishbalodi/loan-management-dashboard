import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { user, isLoading: false } : {};

export function uploadFile(state = initialState, action) {
  
  console.log("inside uploadFile function", action)
  switch (action.type) {
    case userConstants.UPLOAD_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.UPLOAD_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.UPLOAD_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}