import {userConstants} from '../constants/user.constants'

const user=sessionStorage.getItem('user')
const initialState=user? {loggedIn:true,isLoading:false,user }:{}

export function createReminder(state = initialState, action) {
  
  console.log("inside reminder function", action)
  switch (action.type) {
    case userConstants.POST_REMINDER_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.POST_REMINDER_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.POST_REMINDER_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}
export function getReminder(state = initialState, action) {
  
  console.log("inside reminder function", action)
  switch (action.type) {
    case userConstants.GET_REMINDER_REQUEST:
      return {
        user: action.user,
        isLoading : true

      };
    case userConstants.GET_REMINDER_SUCCESS:
      const {res} = action
        console.log("Action", action)
      return {
        res,
        isLoading : false
      };
    case userConstants.GET_REMINDER_FAILURE:
      return {
        isLoading : false
      };
    default:
      return state
  }
}